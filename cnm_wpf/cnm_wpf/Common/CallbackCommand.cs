﻿using System;
using System.Windows.Input;

namespace cnm_wpf.Common
{
    public class CallbackCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;
        private Action<object> callback;

        public CallbackCommand(Action<object> pExecute, Func<object, bool> pCanExecute, Action<object> pCallback)
        {
            this.execute = pExecute;
            this.canExecute = pCanExecute;
            this.callback = pCallback;
        }

        public event EventHandler CanExecuteChanged;
        public void NotifyCanExecuteChanged()
        {
            if (this.CanExecuteChanged != null)
            {
                this.CanExecuteChanged(this, new EventArgs());
            }
        }

        public bool CanExecute(object pArg)
        {
            return this.canExecute(pArg);
        }
        public void Execute(object pArg)
        {
            this.execute(pArg);
            if (this.callback != null)
            {
                this.callback(pArg);
            }
        }
    }
}