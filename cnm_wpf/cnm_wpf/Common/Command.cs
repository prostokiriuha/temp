﻿using System;
using System.Windows.Input;

namespace cnm_wpf.Common
{
    public class Command : ICommand
    {
        private Action<object> executeAction;
        private Func<object, bool> canExecuteAction;

        public Command(Action<object> pExecuteAction)
        {
            this.executeAction = pExecuteAction;
        }
        public Command(Action<object> pExecuteAction, Func<object, bool> pCanExecuteAction)
        {
            this.executeAction = pExecuteAction;
            this.canExecuteAction = pCanExecuteAction;
        }

        public event EventHandler CanExecuteChanged;
        private void NotifyCanExecuteChanged(object sender, EventArgs e)
        {
            this.CanExecuteChanged?.Invoke(sender, e);
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecuteAction == null ? true : this.canExecuteAction(parameter);
        }
        public void Execute(object parameter)
        {
            if (this.executeAction != null) this.executeAction(parameter);
        }
    }
}