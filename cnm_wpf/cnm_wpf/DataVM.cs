﻿using cnm_wpf.ViewModels;
using System.Data;

namespace cnm_wpf
{
    public class DataVM : BaseVM
    {
        private DataTable table;
        public DataTable Table
        {
            get
            {
                return this.table;
            }
            set
            {
                this.table = value;
                base.NotifyPropertyChanged(nameof(this.Table));
            }
        }

        public DataVM(DataTable pTable)
        {
            this.table = pTable;
        }
    }
}