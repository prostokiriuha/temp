﻿using km.cnm;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace cnm_wpf.geometry
{
    public static class GeometryHelper
    {
        /* Static */

        private static readonly Color DEFAULT_COLOR = Color.FromRgb(128, 128, 128);
        private static readonly Color NODE_COLOR = Color.FromRgb(128, 128, 128);
        private static readonly Color SELECTED_PARENT_NODE_COLOR = Color.FromRgb(0, 0, 128);
        private static readonly Color SELECTED_NODE_COLOR = Color.FromRgb(128, 128, 255);
        private static readonly Color MESH_COLOR = Color.FromRgb(128, 128, 128);
        private static readonly Color MESH_BORDER_COLOR = Color.FromRgb(200, 200, 200);
        private static readonly Color SELECTED_MESH_BORDER_COLOR = Color.FromRgb(128, 128, 255);

        /* Fields */

        private static Material textureGridA128Material;

        private static Material defaultMaterial;
        private static Material nodeMaterial;
        private static Material selectedNodeMaterial;
        private static Material selectedParentNodeMaterial;
        private static Material boneMaterial;
        private static Material selectedBoneMaterial;
        private static Material selectedParentBoneMaterial;
        private static Material colliderMaterial;
        private static Material meshMaterial;
        private static Material selectedMeshMaterial;

        /* Properties */



        /* Constructors */



        /* Private methods */

        private static void SetMaterialOpacity(Material pMaterial, double pOpacity)
        {
            if (pMaterial is DiffuseMaterial)
            {
                (pMaterial as DiffuseMaterial).Brush.Opacity = pOpacity;
            }
            else if (pMaterial is SpecularMaterial)
            {
                (pMaterial as SpecularMaterial).Brush.Opacity = pOpacity;
            }
            else if (pMaterial is EmissiveMaterial)
            {
                (pMaterial as EmissiveMaterial).Brush.Opacity = pOpacity;
            }
            else if (pMaterial is MaterialGroup)
            {
                SetMaterialsOpacity(pMaterial as MaterialGroup, pOpacity);
            }
        }
        private static void SetMaterialsOpacity(MaterialGroup pMaterialGroup, double pOpacity)
        {
            foreach (Material material in pMaterialGroup.Children)
            {
                SetMaterialOpacity(material, pOpacity);
            }
        }

        /* Public methods */

        //public static GeometryModel3D NewCubeGeometryModel(Cube cube)
        //{
        //    MeshGeometry3D g = new MeshGeometry3D();

        //    Point3D[] p = new Point3D[]
        //    {
        //        new Point3D(-0.5, -0.5, -0.5),
        //        new Point3D(+0.5, -0.5, -0.5),
        //        new Point3D(-0.5, +0.5, -0.5),
        //        new Point3D(+0.5, +0.5, -0.5),
        //        new Point3D(-0.5, -0.5, +0.5),
        //        new Point3D(+0.5, -0.5, +0.5),
        //        new Point3D(-0.5, +0.5, +0.5),
        //        new Point3D(+0.5, +0.5, +0.5)
        //    };

        //    cSide s = null;

        //    AddSideToGeometry(cube.GetFrontSide(), ref g, p[6], p[7], p[4], p[5]);
        //    AddSideToGeometry(cube.GetBackSide(), ref g, p[3], p[2], p[1], p[0]);
        //    AddSideToGeometry(cube.GetLeftSide(), ref g, p[7], p[3], p[5], p[1]);
        //    AddSideToGeometry(cube.GetRightSide(), ref g, p[2], p[6], p[0], p[4]);
        //    AddSideToGeometry(cube.GetUpSide(), ref g, p[2], p[3], p[6], p[7]);
        //    AddSideToGeometry(cube.GetDownSide(), ref g, p[1], p[0], p[5], p[4]);

        //    GeometryModel3D model = new GeometryModel3D();
        //    model.Geometry = g;
        //    model.Material = GetNodeMaterial();

        //    return model;
        //}
        //private static void AddSideToGeometry(cSide side, ref MeshGeometry3D geometry, Point3D p00, Point3D p10, Point3D p01, Point3D p11)
        //{
        //    if (side != null) geometry.AddPlane(p00, p10, p01, p11, side.GetTex(), side.GetTurn(), side.GetFlip());
        //}

        public static GeometryModel3D NewBoneModel3D()
        {
            MeshGeometry3D geometry = new MeshGeometry3D();

            Point3D[] points = new Point3D[]
            {
                new Point3D(+0, +0, -1),
                new Point3D(+1, +1, +0),
                new Point3D(+1, -1, +0),
                new Point3D(-1, -1, +0),
                new Point3D(-1, +1, +0)
            };

            Point texCoord_00_00 = new Point(0.0, 0.0);
            Point texCoord_10_00 = new Point(1.0, 0.0);
            Point texCoord_05_10 = new Point(0.5, 1.0);

            geometry.AddTriangle(points[0], points[1], points[2], texCoord_05_10, texCoord_00_00, texCoord_10_00);
            geometry.AddTriangle(points[0], points[2], points[3], texCoord_05_10, texCoord_00_00, texCoord_10_00);
            geometry.AddTriangle(points[0], points[3], points[4], texCoord_05_10, texCoord_00_00, texCoord_10_00);
            geometry.AddTriangle(points[0], points[4], points[1], texCoord_05_10, texCoord_00_00, texCoord_10_00);

            GeometryModel3D model = new GeometryModel3D();
            model.Geometry = geometry;
            model.Material = GetBoneMaterial();

            return model;
        }
        //public static GeometryModel3D NewColliderModel3D()
        //{
        //    MeshGeometry3D geometry = new MeshGeometry3D();

        //    Point3D[] points = new Point3D[]
        //    {
        //        new Point3D(-0.5, -0.5, -0.5),
        //        new Point3D(+0.5, -0.5, -0.5),
        //        new Point3D(-0.5, +0.5, -0.5),
        //        new Point3D(+0.5, +0.5, -0.5),
        //        new Point3D(-0.5, -0.5, +0.5),
        //        new Point3D(+0.5, -0.5, +0.5),
        //        new Point3D(-0.5, +0.5, +0.5),
        //        new Point3D(+0.5, +0.5, +0.5)
        //    };

        //    Rect xSidesRect = new Rect(0, 0, 1, 1);
        //    Rect ySidesRect = new Rect(0, 0, 1, 1);
        //    Rect zSidesRect = new Rect(0, 0, 1, 1);
        //    geometry.AddPlane(points[6], points[7], points[4], points[5], zSidesRect);
        //    geometry.AddPlane(points[3], points[2], points[1], points[0], zSidesRect);
        //    geometry.AddPlane(points[2], points[3], points[6], points[7], ySidesRect);
        //    geometry.AddPlane(points[1], points[0], points[5], points[4], ySidesRect);
        //    geometry.AddPlane(points[7], points[3], points[5], points[1], xSidesRect);
        //    geometry.AddPlane(points[2], points[6], points[0], points[4], xSidesRect);

        //    GeometryModel3D model = new GeometryModel3D();
        //    model.Geometry = geometry;
        //    model.Material = GetColliderMaterial();

        //    return model;
        //}
        private static readonly Point3D[] baseCubePoints = new Point3D[]
            {
                new Point3D(0, 0, 0),
                new Point3D(1, 0, 0),
                new Point3D(0, 1, 0),
                new Point3D(1, 1, 0),
                new Point3D(0, 0, 1),
                new Point3D(1, 0, 1),
                new Point3D(0, 1, 1),
                new Point3D(1, 1, 1)
            };
        private static readonly Dictionary<eFace, int[]> baseCubeSideIndices = new Dictionary<eFace, int[]>
        {
            {eFace.Front, new int[] { 3, 2, 1, 0 } },
            {eFace.Back, new int[] { 6, 7, 4, 5 } },
            {eFace.Left, new int[] { 2, 6, 0, 4 } },
            {eFace.Right, new int[] { 7, 3, 5, 1 } },
            {eFace.Up, new int[] { 7, 6, 3, 2 } },
            {eFace.Down, new int[] { 4, 5, 0, 1 } }
        };
        public static GeometryModel3D NewCubeGeometryModel3D(Cube pCube, ImageSource pTextureImage)
        {
            int[] indices = null;

            MeshGeometry3D geometry = new MeshGeometry3D();
            foreach (Side side in pCube.GetSides())
            {
                if (side.GetDraw() != eDraw.None)
                {
                    indices = baseCubeSideIndices[side.GetFace()];
                    geometry.AddPlane(
                        baseCubePoints[indices[0]],
                        baseCubePoints[indices[1]],
                        baseCubePoints[indices[2]],
                        baseCubePoints[indices[3]],
                        side.GetTex(),
                        side.GetTurn(),
                        side.GetFlip());
                }
            }

            GeometryModel3D model = new GeometryModel3D();
            model.Geometry = geometry;

            model.Material = GetCubeMaterial(pTextureImage, pCube.GetBackSide());

            return model;
        }

        public static Material GetDefaultMaterial()
        {
            if (defaultMaterial == null)
            {
                defaultMaterial = new DiffuseMaterial(new SolidColorBrush(DEFAULT_COLOR));
            }

            return defaultMaterial;
        }
        public static Material GetTextureGridA128Material()
        {
            if (textureGridA128Material == null)
            {
                ImageSource texture = new BitmapImage(new Uri("pack://application:,,,/Images/grid_a128.png"));

                Image image = new Image()
                {
                    Source = texture
                };
                VisualBrush brush = new VisualBrush(image)
                {
                    ViewboxUnits = BrushMappingMode.Absolute,
                    Viewbox = new Rect(0, 0, texture.Width, texture.Height),
                    ViewportUnits = BrushMappingMode.Absolute,
                    Viewport = new Rect(0, 0, texture.Width, texture.Height),
                    TileMode = TileMode.Tile
                };
                image.SetValue(RenderOptions.BitmapScalingModeProperty, BitmapScalingMode.NearestNeighbor);
                textureGridA128Material = new DiffuseMaterial(brush);
            }

            return textureGridA128Material;
        }
        //public static Material GetNodeMaterial()
        //{
        //    if (nodeMaterial == null)
        //    {
        //        MaterialGroup materialGroup = new MaterialGroup();
        //        materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(NODE_COLOR)));
        //        materialGroup.Children.Add(GetTextureGridA128Material());
        //        nodeMaterial = materialGroup;
        //    }

        //    return nodeMaterial;
        //}
        public static Material GetSelectedNodeMaterial()
        {
            if (selectedNodeMaterial == null)
            {
                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(SELECTED_PARENT_NODE_COLOR)));
                materialGroup.Children.Add(GetTextureGridA128Material());
                selectedNodeMaterial = materialGroup;
            }

            return selectedNodeMaterial;
        }
        public static Material GetSelectedParentNodeMaterial()
        {
            if (selectedParentNodeMaterial == null)
            {
                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(SELECTED_NODE_COLOR)));
                materialGroup.Children.Add(GetTextureGridA128Material());
                selectedParentNodeMaterial = materialGroup;
            }

            return selectedParentNodeMaterial;
        }
        public static Material GetBoneMaterial()
        {
            if (boneMaterial == null)
            {
                boneMaterial = new DiffuseMaterial(new SolidColorBrush(NODE_COLOR));
            }

            return boneMaterial;
        }
        public static Material GetSelectedBoneMaterial()
        {
            if (selectedBoneMaterial == null)
            {
                selectedBoneMaterial = new DiffuseMaterial(
                    new LinearGradientBrush(
                        new GradientStopCollection()
                        {
                            new GradientStop(SELECTED_PARENT_NODE_COLOR, 0),
                            new GradientStop(SELECTED_NODE_COLOR, 1)
                        },
                        90));
            }

            return selectedBoneMaterial;
        }
        public static Material GetSelectedParentBoneMaterial()
        {
            if (selectedParentBoneMaterial == null)
            {
                selectedParentBoneMaterial = new DiffuseMaterial(new SolidColorBrush(SELECTED_NODE_COLOR));
            }

            return selectedParentBoneMaterial;
        }
        public static Material GetColliderMaterial()
        {
            if (colliderMaterial == null)
            {
                GradientStopCollection gradientStopCollection = new GradientStopCollection()
                {
                    new GradientStop(Colors.Red, 0.01),
                    new GradientStop(Colors.Transparent, 0.05),
                    new GradientStop(Colors.Transparent, 0.95),
                    new GradientStop(Colors.Red, 0.99)
                };

                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(Color.FromArgb(64, 255, 255, 255))));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 0)));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 90)));
                colliderMaterial = materialGroup;
            }

            return colliderMaterial;
        }
        public static Material GetMeshMaterial()
        {
            if (meshMaterial == null)
            {
                GradientStopCollection gradientStopCollection = new GradientStopCollection()
                {
                    new GradientStop(MESH_BORDER_COLOR, 0.01),
                    new GradientStop(Colors.Transparent, 0.05),
                    new GradientStop(Colors.Transparent, 0.95),
                    new GradientStop(MESH_BORDER_COLOR, 0.99)
                };

                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(MESH_COLOR)));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 0)));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 90)));
                meshMaterial = materialGroup;
            }

            return meshMaterial;
        }
        public static Material GetSelectedMeshMaterial()
        {
            if (selectedMeshMaterial == null)
            {
                GradientStopCollection gradientStopCollection = new GradientStopCollection()
                {
                    new GradientStop(MESH_BORDER_COLOR, 0.01),
                    new GradientStop(Colors.Transparent, 0.05),
                    new GradientStop(Colors.Transparent, 0.95),
                    new GradientStop(MESH_BORDER_COLOR, 0.99)
                };

                MaterialGroup materialGroup = new MaterialGroup();
                materialGroup.Children.Add(new DiffuseMaterial(new SolidColorBrush(MESH_COLOR)));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 0)));
                materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 90)));
                selectedMeshMaterial = materialGroup;
            }

            return selectedMeshMaterial;
        }

        public static Material NewTexturedMaterial(ImageSource pImageSource, Side pSide)
        {
            Material material = null;

            ImageBrush brush = new ImageBrush(pImageSource)
            {
                ViewboxUnits = BrushMappingMode.RelativeToBoundingBox,
                Viewbox = new Rect(0, 0, pImageSource.Width, pImageSource.Height),
                ViewportUnits = BrushMappingMode.Absolute,
                Viewport = new Rect(0, 0, pImageSource.Width, pImageSource.Height),
                TileMode = TileMode.Tile
            };
            material = new DiffuseMaterial(brush);

            return material;
        }
        public static Material NewSelectedTexturedMaterial(ImageSource pTexture)
        {
            MaterialGroup materialGroup = new MaterialGroup();

            Image image = new Image()
            {
                Source = pTexture
            };
            VisualBrush brush = new VisualBrush(image)
            {
                ViewboxUnits = BrushMappingMode.Absolute,
                Viewbox = new Rect(0, 0, pTexture.Width, pTexture.Height),
                ViewportUnits = BrushMappingMode.Absolute,
                Viewport = new Rect(0, 0, pTexture.Width, pTexture.Height),
                TileMode = TileMode.Tile
            };
            image.SetValue(RenderOptions.BitmapScalingModeProperty, BitmapScalingMode.NearestNeighbor);

            materialGroup.Children.Add(new DiffuseMaterial(brush));

            GradientStopCollection gradientStopCollection = new GradientStopCollection()
                {
                    new GradientStop(MESH_BORDER_COLOR, 0.01),
                    new GradientStop(Colors.Transparent, 0.05),
                    new GradientStop(Colors.Transparent, 0.95),
                    new GradientStop(MESH_BORDER_COLOR, 0.99)
                };
            materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 0)));
            materialGroup.Children.Add(new DiffuseMaterial(new LinearGradientBrush(gradientStopCollection, 90)));

            return materialGroup;
        }
        public static Material GetCubeMaterial(ImageSource pImageSource, Side pSide)
        {
            return pImageSource == null ? GetMeshMaterial() : NewTexturedMaterial(pImageSource, pSide);
        }
        public static Material GetSelectedCubeMaterial(ImageSource pImageSource, Side pSide)
        {
            return pImageSource == null ? GetSelectedMeshMaterial() : NewTexturedMaterial(pImageSource, pSide);
        }

        public static void SetNodeMaterialsOpacity(int pValue)
        {
            SetMaterialsOpacity(pValue, nodeMaterial, selectedNodeMaterial, selectedParentNodeMaterial);
        }
        public static void SetBoneMaterialsOpacity(int pValue)
        {
            SetMaterialsOpacity(pValue, boneMaterial, selectedBoneMaterial, selectedParentBoneMaterial);
        }
        public static void SetMaterialsOpacity(int pOpacity, params Material[] pMaterials)
        {
            double opacity = (double)pOpacity * 0.01;
            foreach (Material material in pMaterials)
            {
                SetMaterialOpacity(material, opacity);
            }
        }

    }
}