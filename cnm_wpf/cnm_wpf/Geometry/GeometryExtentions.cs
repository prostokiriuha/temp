﻿using System.Windows.Media.Media3D;
using System.Windows;
using km.cnm;
using km.types;

namespace cnm_wpf.geometry
{
    public static class GeometryExtentions
    {
        /* Public methods */

        public static void AddTriangle(this MeshGeometry3D pMesh, Point3D pP0, Point3D pP1, Point3D pP2)
        {
            Point zero = new Point(0, 0);
            AddTriangle(pMesh, pP0, pP1, pP2, zero, zero, zero);
        }
        public static void AddTriangle(this MeshGeometry3D pMesh, Point3D pP0, Point3D pP1, Point3D pP2, Point pTex0, Point pTex1, Point pTex2)
        {
            pMesh.Positions.Add(pP0);
            pMesh.Positions.Add(pP1);
            pMesh.Positions.Add(pP2);

            pMesh.TextureCoordinates.Add(pTex0);
            pMesh.TextureCoordinates.Add(pTex1);
            pMesh.TextureCoordinates.Add(pTex2);

            int si = pMesh.TriangleIndices.Count;
            pMesh.TriangleIndices.Add(si);
            pMesh.TriangleIndices.Add(si + 1);
            pMesh.TriangleIndices.Add(si + 2);
        }
        public static void AddTriangle(this MeshGeometry3D pMesh, Point3D pP0, Point3D pP1, Point3D pP2, Rect pTex)
        {
            pMesh.Positions.Add(pP0);
            pMesh.Positions.Add(pP1);
            pMesh.Positions.Add(pP2);

            pMesh.TextureCoordinates.Add(new Point(pTex.X, pTex.Y));
            pMesh.TextureCoordinates.Add(new Point(pTex.X + pTex.Width, pTex.Y));
            pMesh.TextureCoordinates.Add(new Point(pTex.X + pTex.Width * 0.5, pTex.Y + pTex.Height));

            int si = pMesh.TextureCoordinates.Count;
            pMesh.TriangleIndices.Add(si);
            pMesh.TriangleIndices.Add(si + 1);
            pMesh.TriangleIndices.Add(si + 2);
        }
        public static void AddPlane(this MeshGeometry3D pMesh, Point3D pP00, Point3D pP10, Point3D pP01, Point3D pP11, tex2 pTex, eTurn pTurn, eFlip pFlip)
        {
            pMesh.Positions.Add(pP00);
            pMesh.Positions.Add(pP01);
            pMesh.Positions.Add(pP11);
            pMesh.Positions.Add(pP10);

            double u0 = pTex.u0;
            double u1 = pTex.u1;
            double v0 = pTex.v0;
            double v1 = pTex.v1;

            double[] a = new double[] { u0, u0, u1, u1 };
            double[] b = new double[] { v0, v1, v1, v0 };

            int[] ti =
                pFlip == eFlip.FlipU ? new int[] { 3, 2, 1, 0 } :
                pFlip == eFlip.FlipV ? new int[] { 1, 0, 3, 2 } :
                pFlip == eFlip.FlipO ? new int[] { 2, 3, 0, 1 } :
                new int[] { 0, 1, 2, 3 };
            int dti =
                pTurn == eTurn.Turn90 ? 3 :
                pTurn == eTurn.Turn180 ? 2 :
                pTurn == eTurn.Turn270 ? 1 :
                0;
            int[] i = new int[]
            {
                (ti[0] + dti) % 4,
                (ti[1] + dti) % 4,
                (ti[2] + dti) % 4,
                (ti[3] + dti) % 4
            };

            pMesh.TextureCoordinates.Add(new Point(a[i[0]], b[i[0]]));
            pMesh.TextureCoordinates.Add(new Point(a[i[1]], b[i[1]]));
            pMesh.TextureCoordinates.Add(new Point(a[i[2]], b[i[2]]));
            pMesh.TextureCoordinates.Add(new Point(a[i[3]], b[i[3]]));

            int n = pMesh.TextureCoordinates.Count;
            pMesh.TriangleIndices.Add(n);
            pMesh.TriangleIndices.Add(n + 1);
            pMesh.TriangleIndices.Add(n + 2);
            pMesh.TriangleIndices.Add(n);
            pMesh.TriangleIndices.Add(n + 2);
            pMesh.TriangleIndices.Add(n + 3);
        }
        public static void AddPlane(this MeshGeometry3D pMesh, Point3D pP00, Point3D pP10, Point3D pP01, Point3D pP11, tex2 pTex)
        {
            AddPlane(pMesh, pP00, pP10, pP01, pP11, pTex, eTurn.None, eFlip.None);
        }

    }
}