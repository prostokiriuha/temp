﻿using cnm_wpf.ViewModels;
using Microsoft.Win32;
using System.Windows;

namespace cnm_wpf.Windows
{
    public partial class TexturesWindow : Window
    {
        public TextureWindowVM ViewModel;

        public TexturesWindow(ProjectVM pProject)
        {
            this.ViewModel = new TextureWindowVM(pProject);

            InitializeComponent();

            MainControl.DataContext = this.ViewModel;
        }

        private void TextureLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Filter = "Image|*.jpg;*.jpeg;*.png;*.gif;*.bmp|All files|*.*",
                FilterIndex = 0
            };
            if (dialog.ShowDialog() == true)
            {
                this.ViewModel.Project.Textures.Add(TextureVM.NewFromFile(dialog.FileName));
            }
        }
    }
}