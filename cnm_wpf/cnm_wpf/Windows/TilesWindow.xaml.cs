﻿using km.cnm;
using System.Data;
using System.Windows;

namespace cnm_wpf.Windows
{
    public partial class TilesWindow : Window
    {
        public DataVM Data { get; set; }
        public DataRow SelectedRow { get; set; }

        public TilesWindow(DataVM pData)
        {
            this.Data = pData;

            InitializeComponent();

            MainControl.DataContext = this;
        }

        private void CreateRow_Click(object sender, RoutedEventArgs e)
        {
            DataRow row = this.Data.Table.NewRow();
            row.ItemArray = new object[]
            {
                "", 0, 0, 1, 1, eTurn.None, eFlip.None, eDraw.Full
            };
            this.Data.Table.Rows.Add(row);
            this.SelectedRow = row;
        }

        private void RemoveRow_Click(object sender, RoutedEventArgs e)
        {
            DataRow row = ((sender as FrameworkElement).Tag as DataRowView).Row;

            this.Data.Table.Rows.Remove(row);
        }
    }
}