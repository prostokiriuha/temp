﻿using cnm_wpf.Common;
using cnm_wpf.geometry;
using cnm_wpf.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace cnm_wpf.Windows
{
    public partial class MainWindow : Window
    {
        private const double RADIAN = 57.29577951308233;

        private ProjectVM project;
        public ProjectVM Project
        {
            get
            {
                return this.project;
            }
        }

        private SceneVM scene;
        public SceneVM Scene
        {
            get
            {
                return this.scene;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            //this.InitScrollBoxes();

            this.InitProject();
            this.InitScene();

            MainControl.DataContext = this;
        }

        private void InitProject()
        {
            this.project = new ProjectVM();
        }
        private void InitScene()
        {
            this.scene = new SceneVM();

            Guid id = Guid.Empty;

            const double D = 10;
            Vector3D zero = new Vector3D(0, 0, 0);
            Vector3D up = new Vector3D(0, 1, 0);
            double fow = 60;
            int n = -1;

            id = Guid.NewGuid();
            scene.AddCamera(id, "camera0", new PerspectiveCamera(new Point3D(+D, +D, +D), new Vector3D(-D, -D, -D), up, fow));
            scene.AddCamera(id, "front", new PerspectiveCamera(new Point3D(0, 0, -D), new Vector3D(0, 0, +D), up, fow));
            scene.AddCamera(id, "back", new PerspectiveCamera(new Point3D(0, 0, +D), new Vector3D(0, 0, -D), up, fow));
            scene.AddCamera(id, "left", new PerspectiveCamera(new Point3D(+D, 0, 0), new Vector3D(-D, 0, 0), up, fow));
            scene.AddCamera(id, "right", new PerspectiveCamera(new Point3D(-D, 0, 0), new Vector3D(+D, 0, 0), up, fow));

            CameraComboBox.SelectedIndex = 0;
        }

        //public CameraVM GetSelectedCamera()
        //{
        //    return CameraComboBox.SelectedValue as CameraVM;
        //}

        private static km.cnm.Model NewTestModel(string pName)
        {
            Random random = new Random();

            km.cnm.Model model = new km.cnm.Model(pName);
            model.SetAuthor("silveor");

            int nodeCount = 1;
            for (int ci = 0; ci < nodeCount; ci++)
            {
                km.cnm.Cube cube = new km.cnm.Cube("cube" + ci);
                cube.SetTransform(
                    new km.types.Transform3(
                        new km.types.float3(0, 0, 0),
                        new km.types.float3(0, 0, 0),
                        new km.types.float3(2, 2, 2),
                        new km.types.float3(-1, -1, -1)));
                model.AddCube(cube);
            }

            //const string FILE_PATH = "d:\\test.cnm";
            //km.cnm.Helper.SaveModel(model, FILE_PATH);

            return model;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            km.cnm.Model model = NewTestModel("model0");
            this.project.AddModel(model);

            ModelComboBox.SelectedIndex = 0;
            CubeComboBox.SelectedIndex = 0;
            SideComboBox.SelectedIndex = 0;

            this.ToggleModelRenameMode(false);
            this.ToggleCubeRenameMode(false);
            this.ToggleCameraRenameMode(false);

            //this.SelectTab(null);

            //ModelScrollBox.Items = this.data.Models;

            //ModelScrollBox.GetBindingExpression(ScrollBox.ItemsProperty).UpdateTarget();
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            //this.SelectTab(sender as ToggleButton);
        }

        private void ModelComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ModelVM model = ModelComboBox.SelectedItem as ModelVM;

            //CubeComboBox.ItemsSource = null;
            if (model != null)
            {
                //CubeComboBox.ItemsSource = model.Cubes;
                CubeComboBox.SelectedIndex = 0;
            }
        }

        private void CubeParentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ModelVM model = this.GetSelectedModel();
            CubeVM cube = this.GetSelectedCube();
            if (cube != null)
            {
                CubeVM parentCube =
                    this.CubeParentComboBox.SelectedIndex == 0 ?
                    null :
                    this.CubeParentComboBox.SelectedItem as CubeVM;
                model.SetCubeParent(cube.Id, parentCube == null ? Guid.Empty : parentCube.Id);
            }
        }

        private ModelVM GetSelectedModel()
        {
            return ModelComboBox.SelectedItem as ModelVM;
        }
        private CubeVM GetSelectedCube()
        {
            return CubeComboBox.SelectedItem as CubeVM;
        }
        private SideVM GetSelectedSide()
        {
            return SideComboBox.SelectedItem as SideVM;
        }

        private void ProjectToggleButton_Click(object sender, RoutedEventArgs e)
        {

        }

        // Model methods
        private void ToggleModelRenameMode(bool pIsRenameMode)
        {
            ModelVM model = this.GetSelectedModel();

            if (pIsRenameMode)
            {
                model.NewName = model.Name;

                ModelComboBox.Visibility = Visibility.Hidden;
                ModelNameTextBox.Visibility = Visibility.Visible;
                ModelNameTextBox.Focus();
                ModelNameTextBox.SelectAll();
                ModelCDRButtons.IsEnabled = false;
            }
            else
            {
                ModelNameTextBox.Visibility = Visibility.Hidden;
                ModelComboBox.Visibility = Visibility.Visible;
                ModelComboBox.Focus();
                ModelCDRButtons.IsEnabled = true;

                if (model != null && !string.IsNullOrEmpty(model.NewName))
                {
                    model.Name = this.project.GetNextAvailableName(model, model.NewName);
                }
            }
        }
        // Model events
        private void ModelCDRButtons_CreateClick(object sender, EventArgs e)
        {
            this.project.CreateModel();
            ModelComboBox.SelectedIndex = ModelComboBox.Items.Count - 1;
            this.ToggleModelRenameMode(true);
        }
        private void ModelCDRButtons_DeleteClick(object sender, EventArgs e)
        {
            ModelVM model = this.GetSelectedModel();
            this.project.DeleteModel(model);
            this.ModelComboBox.SelectedIndex = 0;
        }
        private void ModelCDRButtons_RenameClick(object sender, EventArgs e)
        {
            this.ToggleModelRenameMode(true);
        }
        private void ModelNameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.ToggleModelRenameMode(false);
        }
        private void ModelNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ToggleModelRenameMode(false);
            }
            else if (e.Key == Key.Escape)
            {
                ModelVM model = this.GetSelectedModel();
                model.NewName = model.Name;
                this.ToggleModelRenameMode(false);
            }
        }

        // Cube emthods
        private void ToggleCubeRenameMode(bool pIsRenameMode)
        {
            ModelVM model = this.GetSelectedModel();
            CubeVM cube = this.GetSelectedCube();

            if (pIsRenameMode)
            {
                if (cube != null) cube.NewName = cube.Name;

                CubeComboBox.Visibility = Visibility.Hidden;
                CubeNameTextBox.Visibility = Visibility.Visible;
                CubeNameTextBox.Focus();
                CubeNameTextBox.SelectAll();
                CubeCDRButtons.IsEnabled = false;
            }
            else
            {
                CubeNameTextBox.Visibility = Visibility.Hidden;
                CubeComboBox.Visibility = Visibility.Visible;
                CubeComboBox.Focus();
                CubeCDRButtons.IsEnabled = true;

                if (cube != null && !string.IsNullOrEmpty(cube.NewName))
                {
                    cube.Name = model.GetNextAvailableName(cube, cube.NewName);
                }
            }
        }
        private void UpdateCubeParentComboBox()
        {
            ModelVM model = ModelComboBox.SelectedItem as ModelVM;
            CubeVM cube = CubeComboBox.SelectedItem as CubeVM;

            CubeParentComboBox.ItemsSource = null;
            if (cube != null)
            {
                List<CubeVM> availableParentCubes = model.GetAvailableParentCubes(cube.Id);
                availableParentCubes.Insert(0, new CubeVM(null));
                CubeParentComboBox.ItemsSource = availableParentCubes;
                CubeVM parentCube = model.Cubes.FirstOrDefault(x => x.Id == model.GetCubeParentId(cube.Id));
                CubeParentComboBox.SelectedItem = parentCube;
            }
        }
        // Cube events
        private void CubeCDRButtons_CreateClick(object sender, EventArgs e)
        {
            ModelVM model = this.GetSelectedModel();
            model.CreateCube();
            CubeComboBox.SelectedIndex = CubeComboBox.Items.Count - 1;
            this.ToggleCubeRenameMode(true);
        }
        private void CubeCDRButtons_DeleteClick(object sender, EventArgs e)
        {
            ModelVM model = this.GetSelectedModel();
            CubeVM cube = this.GetSelectedCube();
            model.DeleteCube(cube);
            this.ModelComboBox.SelectedIndex = 0;
        }
        private void CubeCDRButtons_RenameClick(object sender, EventArgs e)
        {
            if (this.project.SelectedModel?.SelectedCube == null) return;
            this.ToggleCubeRenameMode(true);
        }
        private void CubeNameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.ToggleCubeRenameMode(false);
        }
        private void CubeNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ToggleCubeRenameMode(false);
            }
            else if (e.Key == Key.Escape)
            {
                CubeVM cube = this.GetSelectedCube();
                cube.NewName = cube.Name;
                this.ToggleCubeRenameMode(false);
            }
        }
        private void CubeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SideComboBox.SelectedIndex = 0;
            this.UpdateCubeParentComboBox();
        }

        // Camera methods
        private void ToggleCameraRenameMode(bool pIsRenameMode)
        {
            CameraVM vmCamera = this.scene.SelectedCamera;

            if (pIsRenameMode)
            {
                vmCamera.NewName = vmCamera.Name;

                CameraComboBox.Visibility = Visibility.Hidden;
                CameraNameTextBox.Visibility = Visibility.Visible;
                CameraNameTextBox.Focus();
                CameraNameTextBox.SelectAll();
                CameraCDRButtons.IsEnabled = false;
            }
            else
            {
                CameraNameTextBox.Visibility = Visibility.Hidden;
                CameraComboBox.Visibility = Visibility.Visible;
                CameraComboBox.Focus();
                CameraCDRButtons.IsEnabled = true;

                if (vmCamera != null && !string.IsNullOrEmpty(vmCamera.NewName))
                {
                    vmCamera.Name = this.scene.GetNextAvailableName(vmCamera, vmCamera.NewName);
                }
            }
        }
        private void UpdateCompasCubeTransform()
        {
            CameraVM cameraVM = this.scene.SelectedCamera;
            if (cameraVM != null)
            {
                double x = cameraVM.DirX;
                double y = cameraVM.DirY;
                double z = cameraVM.DirZ;

                double yaw = Math.Atan2(x, z) * RADIAN;

                double xz = Math.Sqrt(x * x + z * z);
                double pitch = Math.Atan2(-y, xz) * RADIAN;

                Rotation3D rotationX = new AxisAngleRotation3D(new Vector3D(1, 0, 0), -pitch);
                Rotation3D rotationY = new AxisAngleRotation3D(new Vector3D(0, 1, 0), -yaw);

                Matrix3D matrix = new RotateTransform3D(rotationY, 0, 0, 0).Value * new RotateTransform3D(rotationX, 0, 0, 0).Value;
                CompasCubeModelVisual3D.Transform = new MatrixTransform3D(matrix);
            }
        }
        // Camera events
        private void CameraCDRButtons_CreateClick(object sender, EventArgs e)
        {
            this.scene.CreateCamera();
            CameraComboBox.SelectedIndex = CameraComboBox.Items.Count - 1;
            this.ToggleCameraRenameMode(true);
        }
        private void CameraCDRButtons_DeleteClick(object sender, EventArgs e)
        {
            CameraVM camera = this.scene.SelectedCamera;
            this.scene.DeleteCamera(camera);
            this.CameraComboBox.SelectedIndex = 0;
        }
        private void CameraCDRButtons_RenameClick(object sender, EventArgs e)
        {
            this.ToggleCameraRenameMode(true);
        }
        private void CameraNameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.ToggleCameraRenameMode(false);
        }
        private void CameraNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ToggleCameraRenameMode(false);
            }
            else if (e.Key == Key.Escape)
            {
                CameraVM camera = this.scene.SelectedCamera;
                camera.NewName = camera.Name;
                this.ToggleCameraRenameMode(false);
            }
        }

        private void CameraComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.UpdateCompasCubeTransform();
        }
        private void CameraProperty_ValueChanged(object sender, EventArgs e)
        {
            this.UpdateCompasCubeTransform();
        }

        private Dictionary<Guid, GeometryModel3D> cubeGeometries;
        private void BuildModelVisual()
        {
            ModelViewport.Children.Clear();
            this.cubeGeometries = new Dictionary<Guid, GeometryModel3D>();

            foreach (ModelVM vmModel in this.project.Models)
            {
                ModelVisual3D modelVisual = new ModelVisual3D();
                Model3DGroup modelGroup = new Model3DGroup();

                Light light = new AmbientLight(Colors.White);
                modelGroup.Children.Add(light);

                foreach (CubeVM vmCube in vmModel.Cubes)
                {
                    ImageSource textureImage = this.project.GetTexture(vmCube.TextureId)?.ImageSource;
                    GeometryModel3D geometryModel = GeometryHelper.NewCubeGeometryModel3D(vmCube.GetCube(), textureImage);

                    modelGroup.Children.Add(geometryModel);
                    UpdateGeometryTransform(geometryModel, vmCube);
                    this.cubeGeometries.Add(vmCube.Id, geometryModel);
                }

                modelVisual.Content = modelGroup;
                ModelViewport.Children.Add(modelVisual);
            }

            //ModelViewport.Camera = this.scene.SelectedCamera.Camera;
        }
        public void UpdateGeometryModel(CubeVM vmCube)
        {
            if (vmCube == null) return;

            GeometryModel3D geometryModel = this.cubeGeometries[vmCube.Id];
            UpdateGeometryTransform(geometryModel, vmCube);
        }
        private static void UpdateGeometryTransform(GeometryModel3D geometryModel, CubeVM vmCube)
        {
            Transform3DGroup transformGroup = new Transform3DGroup();
            transformGroup.Children.Add(new ScaleTransform3D(vmCube.Transform.SclX, vmCube.Transform.SclY, vmCube.Transform.SclZ));
            transformGroup.Children.Add(new TranslateTransform3D(vmCube.Transform.PvtX, vmCube.Transform.PvtY, vmCube.Transform.PvtZ));
            transformGroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 1, 0), vmCube.Transform.RotY)));
            transformGroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(1, 0, 0), vmCube.Transform.RotX)));
            transformGroup.Children.Add(new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), vmCube.Transform.RotZ)));
            transformGroup.Children.Add(new TranslateTransform3D(vmCube.Transform.PosX, vmCube.Transform.PosY, vmCube.Transform.PosZ));
            geometryModel.Transform = transformGroup;
        }

        private void BuildButton_Click(object sender, RoutedEventArgs e)
        {
            this.BuildModelVisual();
        }

        private void CubeProperty_ValueChanged(object sender, EventArgs e)
        {
            this.UpdateGeometryModel(this.project.SelectedModel.SelectedCube);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ModelNew_Click(object sender, RoutedEventArgs e)
        {
            this.project.SelectedModel = this.project.CreateModel();
        }

        private void ModelLoad_Click(object sender, RoutedEventArgs e)
        {
            //this.project.SelectedModel = this.project.AddModel();
        }

        private void ModelSave_Click(object sender, RoutedEventArgs e)
        {
            ModelVM vmModel = this.project.SelectedModel;
            if (vmModel == null) return;

            SaveFileDialog dialog = new SaveFileDialog()
            {
                Filter = "Cube-Node File|*.cnm|All files|*.*",
                FilterIndex = 0
            };

            if (dialog.ShowDialog() == true)
            {
                km.cnm.Helper.SaveModel(vmModel.Model, dialog.FileName);
            }
        }

        private TexturesWindow texturesWindow;
        private void OpenTexturesWindow(object p)
        {
            if (this.texturesWindow == null)
            {
                this.texturesWindow = new TexturesWindow(this.project) { Owner = this };
                this.texturesWindow.Closed += (_sender, _e) =>
                {
                    this.texturesWindow = null;
                    this.Activate();
                };
            }

            this.texturesWindow.Show();
            this.texturesWindow.Activate();
        }
        private void WindowTextures_Click(object sender, RoutedEventArgs e)
        {
            CubeTextureComboBox.IsMenuOpen = false;
            this.OpenTexturesWindow(null);
        }

        private TilesWindow tilesWindow;
        private void WindowTiles_Click(object sender, RoutedEventArgs e)
        {
            if (this.tilesWindow == null)
            {
                this.tilesWindow = new TilesWindow(this.project.TilesData) { Owner = this };
                this.tilesWindow.Closed += (_sender, _e) => this.tilesWindow = null;
            }

            this.tilesWindow.Show();
            this.tilesWindow.Activate();
        }

    }
}