﻿using System.Windows;
using System.Windows.Controls;

namespace cnm_wpf.Controls
{
    public class PropertyGrid : Grid
    {
        public PropertyGrid()
        {
            this.Loaded += PropertyGrid_Loaded;
        }

        private void PropertyGrid_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Loaded -= PropertyGrid_Loaded;

            const int COLUMN_WIDTH = 48;
            for (int i = 0; i < 4; i++)
            {
                this.ColumnDefinitions.Add(
                    new ColumnDefinition()
                    {
                        Width = new GridLength(COLUMN_WIDTH, GridUnitType.Pixel)
                    });
            }
        }
    }
}