﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cnm_wpf.Controls
{
    public partial class ComboMenu : UserControl
    {
        public static readonly DependencyProperty MenuItemsProperty;
        public static readonly DependencyProperty ItemsSourceProperty;
        public static readonly DependencyProperty SelectedValueProperty;

        public IList<MenuItem> MenuItems
        {
            get
            {
                return (IList<MenuItem>)GetValue(MenuItemsProperty);
            }
            set
            {
                SetValue(MenuItemsProperty, value);
            }
        }
        public IEnumerable ItemsSource
        {
            get
            {
                return (IEnumerable)GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }
        public object SelectedValue
        {
            get
            {
                return (IList<object>)GetValue(SelectedValueProperty);
            }
            set
            {
                SetValue(SelectedValueProperty, value);
            }
        }
        public string DisplayMemberPath
        {
            get
            {
                return ItemsComboBox.DisplayMemberPath;
            }
            set
            {
                ItemsComboBox.DisplayMemberPath = value;
            }
        }
        public string SelectedValuePath
        {
            get
            {
                return ItemsComboBox.SelectedValuePath;
            }
            set
            {
                ItemsComboBox.SelectedValuePath = value;
            }
        }
        public bool IsMenuOpen
        {
            get
            {
                return MenuItemsPopup.IsOpen;
            }
            set
            {
                MenuItemsPopup.IsOpen = value;
            }
        }

        static ComboMenu()
        {
            MenuItemsProperty = DependencyProperty.Register(
                "MenuItems",
                typeof(IList<MenuItem>),
                typeof(ComboMenu));
            ItemsSourceProperty = DependencyProperty.Register(
                "ItemsSource",
                typeof(IEnumerable),
                typeof(ComboMenu));
            SelectedValueProperty = DependencyProperty.Register(
                "SelectedValue",
                typeof(string),
                typeof(ComboMenu));
        }
        public ComboMenu()
        {
            InitializeComponent();

            MainControl.DataContext = this;
        }
    }
}