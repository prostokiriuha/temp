﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace cnm_wpf.Controls
{
    public partial class CDRButtons : UserControl
    {
        //public static readonly DependencyProperty CreateButtonLabelProperty;
        //public string CreateButtonLabel{

        public event EventHandler CreateClick;
        private void NotifyCreateClick()
        {
            if (this.CreateClick != null)
            {
                this.CreateClick(this, new EventArgs());
            }
        }

        public event EventHandler DeleteClick;
        private void NotifyDeleteClick()
        {
            if (this.DeleteClick != null)
            {
                this.DeleteClick(this, new EventArgs());
            }
        }

        public event EventHandler RenameClick;
        private void NotifyRenameClick()
        {
            if (this.RenameClick != null)
            {
                this.RenameClick(this, new EventArgs());
            }
        }

        public CDRButtons()
        {
            InitializeComponent();
        }

        private void CreateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NotifyCreateClick();
        }
        private void DeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NotifyDeleteClick();
        }
        private void RenameButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NotifyRenameClick();
        }
    }
}