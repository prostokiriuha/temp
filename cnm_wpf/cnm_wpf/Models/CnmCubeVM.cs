﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Media3D;

namespace cnm_wpf.viewModels
{
    public class CnmCubeVM : BaseVM
    {
        private km.cnm.Cube cube;
        private CnmTransformVM transform;
        private ObservableCollection<CnmSideVM> sides;

        private GeometryModel3D cubeModel3d;
        private GeometryModel3D CubeModel3d
        {
            get
            {
                return this.cubeModel3d;
            }
        }

        private GeometryModel3D boneModel3D;
        private GeometryModel3D BoneModel3D
        {
            get
            {
                return this.boneModel3D;
            }
        }

        public Guid Id
        {
            get
            {
                return this.cube.GetId();
            }
        }
        public string Name
        {
            get
            {
                return this.cube == null ? null : this.cube.GetName();
            }
            set
            {
                this.cube.SetName(value);
                base.NotifyPropertyChanged("Name");
            }
        }
        public CnmTransformVM Transform
        {
            get
            {
                return this.transform;
            }
        }
        public bool IsNode
        {
            get
            {
                return this.cube.IsNode();
            }
            set
            {
                this.cube.SetIsNode(value);
            }
        }

        private string newName;
        public string NewName
        {
            get
            {
                return this.newName;
            }
            set
            {
                this.newName = value;
                base.NotifyPropertyChanged("NewName");
            }
        }

        public ObservableCollection<CnmSideVM> Sides
        {
            get
            {
                return this.sides;
            }
        }

        public bool IsNull()
        {
            return this.cube == null;
        }

        public CnmCubeVM(km.cnm.Cube pCube)
        {
            this.cube = pCube;

            if (this.cube != null)
            {
                this.transform = new CnmTransformVM(this.cube.GetTransform());

                this.sides = new ObservableCollection<CnmSideVM>();
                this.sides.Add(new CnmSideVM(this.cube.GetFrontSide(), "Front"));
                this.sides.Add(new CnmSideVM(this.cube.GetBackSide(), "Back"));
                this.sides.Add(new CnmSideVM(this.cube.GetLeftSide(), "Left"));
                this.sides.Add(new CnmSideVM(this.cube.GetRightSide(), "Right"));
                this.sides.Add(new CnmSideVM(this.cube.GetUpSide(), "Up"));
                this.sides.Add(new CnmSideVM(this.cube.GetDownSide(), "Down"));

                this.cubeModel3d = new GeometryModel3D();
                this.boneModel3D = new GeometryModel3D();
            }
        }
        
    }
}