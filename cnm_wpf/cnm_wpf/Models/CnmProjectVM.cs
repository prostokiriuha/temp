﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace cnm_wpf.viewModels
{
    public class CnmProjectVM : BaseVM
    {
        private ObservableCollection<CnmModelVM> models;
        public ObservableCollection<CnmModelVM> Models
        {
            get
            {
                return this.models;
            }
            set
            {
                this.models = value;
                base.NotifyPropertyChanged("Models");
            }
        }

        public CnmProjectVM()
        {
            this.models = new ObservableCollection<CnmModelVM>();
        }

        public void AddModel(km.cnm.Model pModel)
        {
            this.models.Add(new CnmModelVM(pModel));
        }
        public void CreateModel()
        {
            km.cnm.Model model = new km.cnm.Model(this.GetNextAvailableName(null, "model"));
            this.AddModel(model);
        }
        public void DeleteModel(CnmModelVM pModel)
        {
            this.models.Remove(pModel);
        }
        public string GetNextAvailableName(CnmModelVM pModel, string pName)
        {
            string name = pName;
            int i = 0;
            while (this.models.FirstOrDefault(model => model != pModel && model.Name == name) != null)
            {
                i++;
                name = pName + i;
            }

            return name;
        }
    }
}