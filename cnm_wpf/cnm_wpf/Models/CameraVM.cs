﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;

namespace cnm_wpf.viewModels
{
    public class CameraVM : BaseVM
    {
        private Guid id;
        public Guid Id
        {
            get
            {
                return this.id;
            }
        }

        private PerspectiveCamera camera;
        public PerspectiveCamera Camera
        {
            get
            {
                return this.camera;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        private string newName;
        public string NewName
        {
            get
            {
                return this.newName;
            }
            set
            {
                this.newName = value;
                base.NotifyPropertyChanged("NewName");
            }
        }

        double posX;
        double posY;
        double posZ;
        public double PosX
        {
            get
            {
                return this.posX;
            }
            set
            {
                this.posX = value;
                base.NotifyPropertyChanged("PosX");
                
                this.UpdateDirection();
                this.UpdateCameraPosition();
            }
        }
        public double PosY
        {
            get
            {
                return this.posY;
            }
            set
            {
                this.posY = value;
                base.NotifyPropertyChanged("PosY");

                this.UpdateDirection();
                this.UpdateCameraPosition();
            }
        }
        public double PosZ
        {
            get
            {
                return this.posZ;
            }
            set
            {
                this.posZ = value;
                base.NotifyPropertyChanged("PosZ");

                this.UpdateDirection();
                this.UpdateCameraPosition();
            }
        }

        double tgtX;
        double tgtY;
        double tgtZ;
        public double TgtX
        {
            get
            {
                return this.tgtX;
            }
            set
            {
                this.tgtX = value;
                base.NotifyPropertyChanged("TgtX");

                this.UpdateDirection();
                this.UpdateCameraLookDirection();
            }
        }
        public double TgtY
        {
            get
            {
                return this.tgtY;
            }
            set
            {
                this.tgtY = value;
                base.NotifyPropertyChanged("TgtY");

                this.UpdateDirection();
                this.UpdateCameraLookDirection();
            }
        }
        public double TgtZ
        {
            get
            {
                return this.tgtZ;
            }
            set
            {
                this.tgtZ = value;
                base.NotifyPropertyChanged("TgtZ");

                this.UpdateDirection();
                this.UpdateCameraLookDirection();
            }
        }

        double dirX;
        double dirY;
        double dirZ;
        public double DirX
        {
            get
            {
                return this.dirX;
            }
            set
            {
                this.dirX = value;
                base.NotifyPropertyChanged("DirX");

                this.UpdateTarget();
                this.UpdateCameraLookDirection();
            }
        }
        public double DirY
        {
            get
            {
                return this.dirY;
            }
            set
            {
                this.dirY = value;
                base.NotifyPropertyChanged("DirY");

                this.UpdateTarget();
                this.UpdateCameraLookDirection();
            }
        }
        public double DirZ
        {
            get
            {
                return this.dirZ;
            }
            set
            {
                this.dirZ = value;
                base.NotifyPropertyChanged("DirZ");

                this.UpdateTarget();
                this.UpdateCameraLookDirection();
            }
        }

        public double FoW
        {
            get
            {
                return this.camera.FieldOfView;
            }
            set
            {
                this.camera.FieldOfView = value;
                base.NotifyPropertyChanged("FOW");
            }
        }

        public CameraVM(Guid pID, string pName, PerspectiveCamera pCamera)
        {
            this.id = pID;
            this.name = pName;
            this.camera = pCamera;

            this.posX = this.camera.Position.X;
            this.posY = this.camera.Position.Y;
            this.posZ = this.camera.Position.Z;

            this.tgtX = this.camera.LookDirection.X;
            this.tgtY = this.camera.LookDirection.Y;
            this.tgtZ = this.camera.LookDirection.Z;

            this.UpdateDirection();
        }

        private void UpdateCameraPosition()
        {
            this.camera.Position = new Point3D(this.posX, this.posY, this.posZ);
        }
        private void UpdateCameraLookDirection()
        {
            this.camera.LookDirection = new Vector3D(this.tgtX, this.tgtY, this.tgtZ);
        }
        private void UpdateDirection()
        {
            this.dirX = this.tgtX - this.posX;
            this.dirY = this.tgtY - this.posY;
            this.dirZ = this.tgtZ - this.posZ;

            this.NotifyPropertyChanged("DirX");
            this.NotifyPropertyChanged("DirY");
            this.NotifyPropertyChanged("DirZ");
        }
        private void UpdateTarget()
        {
            this.tgtX = this.posX + this.dirX;
            this.tgtY = this.posY + this.dirY;
            this.tgtZ = this.posZ + this.dirZ;

            this.NotifyPropertyChanged("TgtX");
            this.NotifyPropertyChanged("TgtY");
            this.NotifyPropertyChanged("TgtZ");
        }
    }
}