﻿using km.cnm;

namespace cnm_wpf.viewModels
{
    public class CnmSideVM : BaseVM
    {
        private cSide side;

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public float U0
        {
            get
            {
                return this.side.GetU0();
            }
            set
            {
                this.side.SetU0(value);
            }
        }
        public float V0
        {
            get
            {
                return this.side.GetV0();
            }
            set
            {
                this.side.SetV0(value);
            }
        }
        public float U1
        {
            get
            {
                return this.side.GetU1();
            }
            set
            {
                this.side.SetU1(value);
            }
        }
        public float V1
        {
            get
            {
                return this.side.GetV1();
            }
            set
            {
                this.side.SetV1(value);
            }
        }
        public eTurn Turn
        {
            get
            {
                return this.side.GetTurn();
            }
            set
            {
                this.side.SetTurn(value);
            }
        }
        public eFlip Flip
        {
            get
            {
                return this.side.GetFlip();
            }
            set
            {
                this.side.SetFlip(value);
            }
        }
        public eDraw Draw
        {
            get
            {
                return this.side.GetDraw();
            }
            set
            {
                this.side.SetDraw(value);
            }
        }

        public CnmSideVM(cSide pSide, string pName)
        {
            this.side = pSide;
            this.name = pName;
        }
    }
}