﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace cnm_wpf.viewModels
{
    public class CnmModelVM : BaseVM
    {
        private km.cnm.Model model;
        private ObservableCollection<CnmCubeVM> cubes;

        public km.cnm.Model Model
        {
            get
            {
                return this.model;
            }
        }
        public ObservableCollection<CnmCubeVM> Cubes
        {
            get
            {
                return this.cubes;
            }
        }

        public string Name
        {
            get
            {
                return this.model.GetName();
            }
            set
            {
                this.model.SetName(value);
                base.NotifyPropertyChanged("Name");
            }
        }

        private string newName;
        public string NewName
        {
            get
            {
                return this.newName;
            }
            set
            {
                this.newName = value;
                base.NotifyPropertyChanged("NewName");
            }
        }

        public string Author
        {
            get
            {
                return this.model.GetAuthor();
            }
            set
            {
                this.model.SetAuthor(value);
                base.NotifyPropertyChanged("Author");
            }
        }

        public CnmModelVM(km.cnm.Model pModel)
        {
            this.model = pModel;

            this.cubes = new ObservableCollection<CnmCubeVM>(
                this.model.GetCubes()
                .Select(cube => new CnmCubeVM(cube)));
        }

        public void AddCube(km.cnm.Cube pCube)
        {
            this.model.AddCube(pCube);
            this.cubes.Add(new CnmCubeVM(pCube));
        }
        public void CreateCube()
        {
            km.cnm.Cube cube = new km.cnm.Cube(this.GetNextAvailableName(null, "cube"));
            this.AddCube(cube);
        }
        public void DeleteCube(CnmCubeVM pCube)
        {
            this.cubes.Remove(pCube);
            this.model.DeleteCube(pCube.Id);
        }

        public Guid GetCubeParentId(Guid pCubeId)
        {
            return this.model.GetCubeParentId(pCubeId);
        }
        public void SetCubeParent(Guid pCubeId, Guid pParentId)
        {
            this.model.SetCubeParent(pCubeId, pParentId);
        }

        public string GetNextAvailableName(CnmCubeVM pCube, string pName)
        {
            string name = pName;
            int i = 0;
            while (this.cubes.FirstOrDefault(cube => cube != pCube && cube.Name == name) != null)
            {
                i++;
                name = pName + i;
            }

            return name;
        }

        public List<CnmCubeVM> GetAvailableParentCubes(Guid pCubeId)
        {
            return
                this.model.GetAvailableParentIds(pCubeId)
                .Select(id => this.cubes.FirstOrDefault(cube => cube.Id == id))
                .ToList();
        }
        public string GetAvailableParentName(string pName)
        {
            return this.model.GetAvailableCubeName(pName);
        }
    }
}