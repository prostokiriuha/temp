﻿using km.cnm;
using System;

namespace cnm_wpf.ViewModels
{
    public class SideVM : BaseVM
    {
        private Side side;

        private string name;
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public float U0
        {
            get
            {
                return this.side.GetU0();
            }
            set
            {
                this.side.SetU0(value);
            }
        }
        public float V0
        {
            get
            {
                return this.side.GetV0();
            }
            set
            {
                this.side.SetV0(value);
            }
        }
        public float U1
        {
            get
            {
                return this.side.GetU1();
            }
            set
            {
                this.side.SetU1(value);
            }
        }
        public float V1
        {
            get
            {
                return this.side.GetV1();
            }
            set
            {
                this.side.SetV1(value);
            }
        }
        public eTurn Turn
        {
            get
            {
                return this.side.GetTurn();
            }
            set
            {
                this.side.SetTurn(value);
            }
        }
        public eFlip Flip
        {
            get
            {
                return this.side.GetFlip();
            }
            set
            {
                this.side.SetFlip(value);
            }
        }
        public eDraw Draw
        {
            get
            {
                return this.side.GetDraw();
            }
            set
            {
                this.side.SetDraw(value);
            }
        }

        public bool FlipU
        {
            get
            {
                eFlip flip = this.side.GetFlip();
                return flip == eFlip.FlipU || flip == eFlip.FlipO;
            }
            set
            {
                eFlip flip = this.side.GetFlip();
                this.side.SetFlip(value ? flip == eFlip.None ? eFlip.FlipU : eFlip.FlipO : flip == eFlip.None ? eFlip.None : eFlip.FlipV);
            }
        }
        public bool FlipV
        {
            get
            {
                eFlip flip = this.side.GetFlip();
                return flip == eFlip.FlipV || flip == eFlip.FlipO;
            }
            set
            {
                eFlip flip = this.side.GetFlip();
                this.side.SetFlip(value ? flip == eFlip.None ? eFlip.FlipV : eFlip.FlipO : flip == eFlip.None ? eFlip.None : eFlip.FlipU);
            }
        }

        public SideVM(Side pSide, string pName)
        {
            this.side = pSide;
            this.name = pName;
        }
    }
}