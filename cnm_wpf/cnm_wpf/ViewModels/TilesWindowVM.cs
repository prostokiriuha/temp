﻿namespace cnm_wpf.ViewModels
{
    public class TilesWindowVM : WindowVM
    {
        public SideVM selectedSide;
        public SideVM SelectedSide
        {
            get
            {
                return this.selectedSide;
            }
            set
            {
                this.selectedSide = value;
                base.NotifyPropertyChanged(nameof(this.SelectedSide));
            }
        }

        public TilesWindowVM(ProjectVM pProject) : base(pProject)
        {
        }
    }
}