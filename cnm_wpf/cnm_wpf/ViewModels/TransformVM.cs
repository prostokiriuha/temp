﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cnm_wpf.ViewModels
{
    public class TransformVM : BaseVM
    {
        private km.types.Transform3 transform;

        public float PosX
        {
            get
            {
                return this.transform.Position.x;
            }
            set
            {
                this.transform.Position.x = value;
            }
        }
        public float PosY
        {
            get
            {
                return this.transform.Position.y;
            }
            set
            {
                this.transform.Position.y = value;
            }
        }
        public float PosZ
        {
            get
            {
                return this.transform.Position.z;
            }
            set
            {
                this.transform.Position.z = value;
            }
        }

        public float RotX
        {
            get
            {
                return this.transform.Rotation.x;
            }
            set
            {
                this.transform.Rotation.x = value;
            }
        }
        public float RotY
        {
            get
            {
                return this.transform.Rotation.y;
            }
            set
            {
                this.transform.Rotation.y = value;
            }
        }
        public float RotZ
        {
            get
            {
                return this.transform.Rotation.z;
            }
            set
            {
                this.transform.Rotation.z = value;
            }
        }

        public float SclX
        {
            get
            {
                return this.transform.Scale.x;
            }
            set
            {
                this.transform.Scale.x = value;
            }
        }
        public float SclY
        {
            get
            {
                return this.transform.Scale.y;
            }
            set
            {
                this.transform.Scale.y = value;
            }
        }
        public float SclZ
        {
            get
            {
                return this.transform.Scale.z;
            }
            set
            {
                this.transform.Scale.z = value;
            }
        }

        public float PvtX
        {
            get
            {
                return this.transform.Pivot.x;
            }
            set
            {
                this.transform.Pivot.x = value;
            }
        }
        public float PvtY
        {
            get
            {
                return this.transform.Pivot.y;
            }
            set
            {
                this.transform.Pivot.y = value;
            }
        }
        public float PvtZ
        {
            get
            {
                return this.transform.Pivot.z;
            }
            set
            {
                this.transform.Pivot.z = value;
            }
        }

        public TransformVM(km.types.Transform3 pTransform)
        {
            this.transform = pTransform;
        }
    }
}