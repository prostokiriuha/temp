﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Media3D;

namespace cnm_wpf.ViewModels
{
    public class SceneVM : BaseVM
    {
        private ObservableCollection<CameraVM> cameras;
        public ObservableCollection<CameraVM> Cameras
        {
            get
            {
                return this.cameras;
            }
        }

        private CameraVM selectedCamera;
        public CameraVM SelectedCamera
        {
            get
            {
                return this.selectedCamera;
            }
            set
            {
                this.selectedCamera = value;
                this.NotifyPropertyChanged(nameof(this.SelectedCamera));
            }
        }

        private Dictionary<int, object> cubeMeshes;
        public IEnumerable Meshes
        {
            get
            {
                return null;
            }
        }

        public SceneVM()
        {
            this.cameras = new ObservableCollection<CameraVM>();
        }

        public void AddCamera(Guid pID, string pName, PerspectiveCamera pCamera)
        {
            this.cameras.Add(new CameraVM(pID, this.GetNextAvailableName(null, pName), pCamera));
        }
        public void CreateCamera()
        {
            PerspectiveCamera camera = new PerspectiveCamera(new Point3D(5, 10, 10), new Vector3D(0, 0, 0), new Vector3D(0, 1, 0), 90);
            this.AddCamera(Guid.NewGuid(), "camera", camera);
        }
        public void DeleteCamera(CameraVM pCamera)
        {
            this.cameras.Remove(pCamera);
        }

        public string GetNextAvailableName(CameraVM pCamera, string pName)
        {
            string name = pName;
            int i = 0;
            while (this.cameras.FirstOrDefault(cube => cube != pCamera && cube.Name == name) != null)
            {
                i++;
                name = pName + i;
            }

            return name;
        }
    }
}