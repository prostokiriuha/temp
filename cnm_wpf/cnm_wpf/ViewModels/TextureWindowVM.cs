﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cnm_wpf.ViewModels
{
    public class TextureWindowVM : WindowVM
    {
        public TextureVM selectedTexture;
        public TextureVM SelectedTexture
        {
            get
            {
                return this.selectedTexture;
            }
            set
            {
                this.selectedTexture = value;
                base.NotifyPropertyChanged(nameof(this.SelectedTexture));
            }
        }

        public TextureWindowVM(ProjectVM pProject) : base(pProject)
        {
        }
    }
}