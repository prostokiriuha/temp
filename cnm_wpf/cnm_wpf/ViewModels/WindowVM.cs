﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cnm_wpf.ViewModels
{
    public class WindowVM : BaseVM
    {
        private ProjectVM project;
        public ProjectVM Project
        {
            get
            {
                return this.project;
            }
            set
            {
                this.project = value;
                base.NotifyPropertyChanged(nameof(this.Project));
            }
        }

        public WindowVM(ProjectVM pProject)
        {
            this.project = pProject;
        }
    }
}