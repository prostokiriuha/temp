﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace cnm_wpf.ViewModels
{
    public class TextureVM : BaseVM
    {
        private Guid id;
        public Guid Id
        {
            get
            {
                return this.id;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                base.NotifyPropertyChanged(nameof(this.Name));
            }
        }

        private string filePath;
        public string FilePath
        {
            get
            {
                return this.filePath;
            }
            set
            {
                this.filePath = value;
                base.NotifyPropertyChanged(nameof(this.FilePath));
            }
        }

        private ImageSource imageSource;
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
            set
            {
                this.imageSource = value;
                base.NotifyPropertyChanged(nameof(this.ImageSource));
            }
        }

        public TextureVM(string pFilePath, ImageSource pImageSource)
        {
            this.FilePath = pFilePath;
            this.ImageSource = pImageSource;

            this.id = Guid.NewGuid();
            this.Name = Path.GetFileNameWithoutExtension(this.filePath);
        }

        public static TextureVM NewFromFile(string filePath)
        {
            return new TextureVM(filePath, new BitmapImage(new Uri(filePath)));
        }
    }
}