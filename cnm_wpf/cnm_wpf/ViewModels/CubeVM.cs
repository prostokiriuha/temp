﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Media3D;

namespace cnm_wpf.ViewModels
{
    public class CubeVM : BaseVM
    {
        private km.cnm.Cube cube;
        private TransformVM transform;

        private GeometryModel3D cubeModel3d;
        private GeometryModel3D CubeModel3d
        {
            get
            {
                return this.cubeModel3d;
            }
        }

        private GeometryModel3D boneModel3D;
        private GeometryModel3D BoneModel3D
        {
            get
            {
                return this.boneModel3D;
            }
        }

        private Guid textureId;
        public Guid TextureId
        {
            get
            {
                return this.textureId;
            }
            set
            {
                this.textureId = value;
                this.NotifyPropertyChanged(nameof(this.TextureId));
            }
        }

        public Guid Id
        {
            get
            {
                return this.cube.GetId();
            }
        }
        public string Name
        {
            get
            {
                return this.cube == null ? null : this.cube.GetName();
            }
            set
            {
                this.cube.SetName(value);
                base.NotifyPropertyChanged("Name");
            }
        }
        public TransformVM Transform
        {
            get
            {
                return this.transform;
            }
        }
        public bool IsNode
        {
            get
            {
                return this.cube.IsNode();
            }
            set
            {
                this.cube.SetIsNode(value);
            }
        }

        private string newName;
        public string NewName
        {
            get
            {
                return this.newName;
            }
            set
            {
                this.newName = value;
                base.NotifyPropertyChanged("NewName");
            }
        }

        private ObservableCollection<SideVM> sides;
        public ObservableCollection<SideVM> Sides
        {
            get
            {
                return this.sides;
            }
        }

        private SideVM selectedSide;
        public SideVM SelectedSide
        {
            get
            {
                return this.selectedSide;
            }
            set
            {
                this.selectedSide = value;
                base.NotifyPropertyChanged(nameof(this.SelectedSide));
            }
        }

        public bool IsNull()
        {
            return this.cube == null;
        }

        public CubeVM(km.cnm.Cube pCube)
        {
            this.cube = pCube;

            if (this.cube != null)
            {
                this.transform = new TransformVM(this.cube.GetTransform());

                this.sides = new ObservableCollection<SideVM>();
                this.sides.Add(new SideVM(this.cube.GetFrontSide(), "Front"));
                this.sides.Add(new SideVM(this.cube.GetBackSide(), "Back"));
                this.sides.Add(new SideVM(this.cube.GetLeftSide(), "Left"));
                this.sides.Add(new SideVM(this.cube.GetRightSide(), "Right"));
                this.sides.Add(new SideVM(this.cube.GetUpSide(), "Up"));
                this.sides.Add(new SideVM(this.cube.GetDownSide(), "Down"));

                this.cubeModel3d = new GeometryModel3D();
                this.boneModel3D = new GeometryModel3D();
            }
        }

        public km.cnm.Cube GetCube()
        {
            return this.cube;
        }

    }
}