﻿using km.cnm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;

namespace cnm_wpf.ViewModels
{
    public class ProjectVM : BaseVM
    {
        private ObservableCollection<ModelVM> models;
        public ObservableCollection<ModelVM> Models
        {
            get
            {
                return this.models;
            }
            set
            {
                this.models = value;
                base.NotifyPropertyChanged("Models");
            }
        }

        private ModelVM selectedModel;
        public ModelVM SelectedModel
        {
            get
            {
                return this.selectedModel;
            }
            set
            {
                this.selectedModel = value;
                base.NotifyPropertyChanged(nameof(this.SelectedModel));
            }
        }

        private ObservableCollection<TextureVM> textures;
        public ObservableCollection<TextureVM> Textures
        {
            get
            {
                return this.textures;
            }
            set
            {
                this.textures = value;
                base.NotifyPropertyChanged(nameof(this.Textures));
            }
        }

        private ObservableCollection<SideVM> tiles;
        public ObservableCollection<SideVM> Tiles
        {
            get
            {
                return this.tiles;
            }
            set
            {
                this.tiles = value;
                base.NotifyPropertyChanged(nameof(this.Tiles));
            }
        }

        private DataSet dataSet;

        private const string TILES_DATA = "tiles";
        public DataVM TilesData { get { return new DataVM(this.dataSet.Tables[TILES_DATA]); } }

        private void InitDataSet()
        {
            this.dataSet = new DataSet();

            DataTable table = null;

            table = this.dataSet.Tables.Add(TILES_DATA);
            table.Columns.Add("NAME", typeof(string));
            table.Columns.Add("U0", typeof(double));
            table.Columns.Add("V0", typeof(double));
            table.Columns.Add("U1", typeof(double));
            table.Columns.Add("V1", typeof(double));
            table.Columns.Add("TURN", typeof(eTurn));
            table.Columns.Add("FLIP", typeof(eFlip));
            table.Columns.Add("DRAW", typeof(eDraw));

            //

            table.Rows.Add("", 0, 0, 1, 1, eTurn.None, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 1, 1, eTurn.Turn180, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 1, 1, eTurn.Turn270, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 1, 1, eTurn.Turn90, eFace.None, eDraw.None);

            table.Rows.Add("", 0, 0, 0.5, 0.5, eTurn.None, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 0.5, 0.5, eTurn.Turn180, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 0.5, 0.5, eTurn.Turn270, eFace.None, eDraw.None);
            table.Rows.Add("", 0, 0, 0.5, 0.5, eTurn.Turn90, eFace.None, eDraw.None);
        }

        //private TextureVM selectedTexture;
        //public TextureVM SelectedTexture
        //{
        //    get
        //    {
        //        return this.selectedModel;
        //    }
        //    set
        //    {
        //        this.selectedModel = value;
        //        base.NotifyPropertyChanged(nameof(this.SelectedModel));
        //    }
        //}

        public ProjectVM()
        {
            this.models = new ObservableCollection<ModelVM>();
            this.textures = new ObservableCollection<TextureVM>();

            this.InitDataSet();
        }

        public ModelVM AddModel(km.cnm.Model pModel)
        {
            ModelVM vmModel = new ModelVM(pModel);
            this.models.Add(vmModel);
            return vmModel;
        }
        public ModelVM CreateModel()
        {
            km.cnm.Model model = new km.cnm.Model(this.GetNextAvailableName(null, "model"));
            return this.AddModel(model);
        }
        public void DeleteModel(ModelVM pModel)
        {
            this.models.Remove(pModel);
        }
        public string GetNextAvailableName(ModelVM pModel, string pName)
        {
            string name = pName;
            int i = 0;
            while (this.models.FirstOrDefault(model => model != pModel && model.Name == name) != null)
            {
                i++;
                name = pName + i;
            }

            return name;
        }
        public TextureVM GetTexture(Guid pId)
        {
            return this.textures.FirstOrDefault(x => x.Id == pId);
        }
    }
}