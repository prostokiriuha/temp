﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace km.wpf.controls
{
	public partial class MultyValueEditBlock : UserControl
	{
		/* Enums */

		private enum ValueIndex : byte
		{
			None = 0,
			Value1 = 1,
			Value2 = 2,
			Value3 = 3,
			Value4 = 4
		}

		/* Const */

		private const string DEFAULT_VALUES_SEPARATOR = ", ";

		/* Static */

		public static readonly DependencyProperty Value1Property;
		public static readonly DependencyProperty Value2Property;
		public static readonly DependencyProperty Value3Property;
		public static readonly DependencyProperty Value4Property;

		public static readonly DependencyProperty Label1Property;
		public static readonly DependencyProperty Label2Property;
		public static readonly DependencyProperty Label3Property;
		public static readonly DependencyProperty Label4Property;

		NumberFormatInfo defaultNumberFormatInfo = new NumberFormatInfo()
		{
			CurrencyDecimalDigits = 1,
			CurrencyDecimalSeparator = ".",
			NaNSymbol = "[nan]"
		};

		/* Fields */

		public double Value1
		{
			get
			{
				return (double)GetValue(Value1Property);
			}
			set
			{
				SetValue(Value1Property, value);
			}
		}
		public double Value2
		{
			get
			{
				return (double)GetValue(Value2Property);
			}
			set
			{
				SetValue(Value2Property, value);
			}
		}
		public double Value3
		{
			get
			{
				return (double)GetValue(Value3Property);
			}
			set
			{
				SetValue(Value3Property, value);
			}
		}
		public double Value4
		{
			get
			{
				return (double)GetValue(Value4Property);
			}
			set
			{
				SetValue(Value4Property, value);
			}
		}

		public string Label1
		{
			get
			{
				return (string)GetValue(Label1Property);
			}
			set
			{
				SetValue(Label1Property, value);
			}
		}
		public string Label2
		{
			get
			{
				return (string)GetValue(Label2Property);
			}
			set
			{
				SetValue(Label2Property, value);
			}
		}
		public string Label3
		{
			get
			{
				return (string)GetValue(Label3Property);
			}
			set
			{
				SetValue(Label3Property, value);
			}
		}
		public string Label4
		{
			get
			{
				return (string)GetValue(Label4Property);
			}
			set
			{
				SetValue(Label4Property, value);
			}
		}

		private ValueIndex selectedValueIndex;

		private string valuesSeparator;
		private int valuesCount;

		/* Properties */

		public string Separator
		{
			set
			{
				this.valuesSeparator = value;
			}
		}
		public int ValuesCount
		{
			get
			{
				return this.valuesCount;
			}
			set
			{
				this.valuesCount = value < 1 ? 1 : value > 4 ? 4 : value;

				this.UpdateValueControlsVisibility();
			}
		}

		/* Constructors */

		static MultyValueEditBlock()
		{
			Value1Property = DependencyProperty.Register("Value1", typeof(double), typeof(MultyValueEditBlock));
			Value2Property = DependencyProperty.Register("Value2", typeof(double), typeof(MultyValueEditBlock));
			Value3Property = DependencyProperty.Register("Value3", typeof(double), typeof(MultyValueEditBlock));
			Value4Property = DependencyProperty.Register("Value4", typeof(double), typeof(MultyValueEditBlock));

			Label1Property = DependencyProperty.Register("Label1", typeof(string), typeof(MultyValueEditBlock));
			Label2Property = DependencyProperty.Register("Label2", typeof(string), typeof(MultyValueEditBlock));
			Label3Property = DependencyProperty.Register("Label3", typeof(string), typeof(MultyValueEditBlock));
			Label4Property = DependencyProperty.Register("Label4", typeof(string), typeof(MultyValueEditBlock));
		}
		public MultyValueEditBlock()
		{
			InitializeComponent();

			MainGrid.DataContext = this;
			this.valuesSeparator = DEFAULT_VALUES_SEPARATOR;
		}

		/* Private methods */

		private void SetValueByIndex(ValueIndex pValueIndex, double pValue, bool pIsUpdateControls)
		{
			switch (pValueIndex)
			{
				case ValueIndex.Value1:
					this.Value1 = pValue;
					if (pIsUpdateControls)
					{
						Value1TextBlock.Text = this.GetValueString(ValueIndex.Value1);
						Value1TextBox.Text = this.GetValueString(ValueIndex.Value1);
					}
					break;

				case ValueIndex.Value2:
					this.Value2 = pValue;
					if (pIsUpdateControls)
					{
						Value2TextBlock.Text = this.GetValueString(ValueIndex.Value2);
						Value2TextBox.Text = this.GetValueString(ValueIndex.Value2);
					}
					break;

				case ValueIndex.Value3:
					this.Value3 = pValue;
					if (pIsUpdateControls)
					{
						Value3TextBlock.Text = this.GetValueString(ValueIndex.Value3);
						Value3TextBox.Text = this.GetValueString(ValueIndex.Value3);
					}
					break;

				case ValueIndex.Value4:
					this.Value4 = pValue;
					if (pIsUpdateControls)
					{
						Value4TextBlock.Text = this.GetValueString(ValueIndex.Value4);
						Value4TextBox.Text = this.GetValueString(ValueIndex.Value4);
					}
					break;
			}
		}
		private double GetValueByIndex(ValueIndex pValueIndex)
		{
			return
				pValueIndex == ValueIndex.Value1 ? this.Value1 :
				pValueIndex == ValueIndex.Value2 ? this.Value2 :
				pValueIndex == ValueIndex.Value3 ? this.Value3 :
				pValueIndex == ValueIndex.Value4 ? this.Value4 :
				double.NaN;
		}
		private string GetValueString(ValueIndex pValueIndex)
		{
			return this.GetValueByIndex(pValueIndex).ToString(defaultNumberFormatInfo);
		}

		private void UpdateValuesTextBlock()
		{
			string separator = ", ";
			ValuesTextBlock.Text =
				this.valuesCount == 4 ? string.Format(
					"{1}{0}{2}{0}{3}{0}{4}",
					separator,
					this.GetValueString(ValueIndex.Value1),
					this.GetValueString(ValueIndex.Value2),
					this.GetValueString(ValueIndex.Value3),
					this.GetValueString(ValueIndex.Value4)) :
				this.valuesCount == 3 ? string.Format(
					"{1}{0}{2}{0}{3}",
					separator,
					this.GetValueString(ValueIndex.Value1),
					this.GetValueString(ValueIndex.Value2),
					this.GetValueString(ValueIndex.Value3)) :
				this.valuesCount == 2 ? string.Format(
					"{1}{0}{2}",
					separator,
					this.GetValueString(ValueIndex.Value1),
					this.GetValueString(ValueIndex.Value2)) :
				this.GetValueString(ValueIndex.Value1);
		}
		private void UpdateValueControlsVisibility()
		{
			Label2TextBlock.Visibility = this.valuesCount < 2 ? Visibility.Collapsed : Visibility.Visible;
			Value2TextBlock.Visibility = this.valuesCount < 2 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value2 ? Visibility.Hidden : Visibility.Visible;
			Value2TextBox.Visibility = this.valuesCount < 2 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value2 ? Visibility.Visible : Visibility.Hidden;

			Label3TextBlock.Visibility = this.valuesCount < 3 ? Visibility.Collapsed : Visibility.Visible;
			Value3TextBlock.Visibility = this.valuesCount < 3 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value3 ? Visibility.Hidden : Visibility.Visible;
			Value3TextBox.Visibility = this.valuesCount < 3 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value3 ? Visibility.Visible : Visibility.Hidden;

			Label4TextBlock.Visibility = this.valuesCount < 4 ? Visibility.Collapsed : Visibility.Visible;
			Value4TextBlock.Visibility = this.valuesCount < 4 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value4 ? Visibility.Hidden : Visibility.Visible;
			Value4TextBox.Visibility = this.valuesCount < 4 ? Visibility.Collapsed : this.selectedValueIndex == ValueIndex.Value4 ? Visibility.Visible : Visibility.Hidden;
		}

		private void BeginEditValue(ValueIndex pValueIndex, TextBlock pValueTextBlock, TextBox pValueTextBox)
		{
			pValueTextBlock.Visibility = System.Windows.Visibility.Hidden;
			pValueTextBox.Visibility = System.Windows.Visibility.Visible;

			pValueTextBox.Text = this.GetValueByIndex(pValueIndex).ToString(defaultNumberFormatInfo);
			pValueTextBox.Focus();
			pValueTextBox.SelectAll();

			this.selectedValueIndex = pValueIndex;
		}
		private void EndEditValue(ValueIndex pValueIndex, TextBlock pValueTextBlock, TextBox pValueTextBox, bool pIsSaveChanges)
		{
			if (pIsSaveChanges)
			{
				double newValue = double.NaN;
				if (Double.TryParse(pValueTextBox.Text, out newValue))
				{
					this.SetValueByIndex(pValueIndex, newValue, true);
				}
			}

			pValueTextBox.Text = null;
			pValueTextBox.Visibility = System.Windows.Visibility.Hidden;
			pValueTextBlock.Visibility = System.Windows.Visibility.Visible;

			this.selectedValueIndex = ValueIndex.None;
		}
		private void SwitchToEditValue(ValueIndex pValueIndex, bool pIsSaveChanges)
		{
			this.EndEditValue(ValueIndex.Value1, Value1TextBlock, Value1TextBox, pIsSaveChanges);

			if (this.valuesCount >= 2)
			{
				this.EndEditValue(ValueIndex.Value2, Value2TextBlock, Value2TextBox, pIsSaveChanges);
			}

			if (this.valuesCount >= 3)
			{
				this.EndEditValue(ValueIndex.Value3, Value3TextBlock, Value3TextBox, pIsSaveChanges);
			}

			if (this.valuesCount >= 4)
			{
				this.EndEditValue(ValueIndex.Value4, Value4TextBlock, Value4TextBox, pIsSaveChanges);
			}

			switch (pValueIndex)
			{
				case ValueIndex.Value1:
					this.BeginEditValue(ValueIndex.Value1, Value1TextBlock, Value1TextBox);
					break;

				case ValueIndex.Value2:
					this.BeginEditValue(ValueIndex.Value2, Value2TextBlock, Value2TextBox);
					break;

				case ValueIndex.Value3:
					this.BeginEditValue(ValueIndex.Value3, Value3TextBlock, Value3TextBox);
					break;

				case ValueIndex.Value4:
					this.BeginEditValue(ValueIndex.Value4, Value4TextBlock, Value4TextBox);
					break;
			}

			this.UpdateValuesTextBlock();
			this.UpdateValueControlsVisibility();
		}

		private void ProcessValueTextBoxKeyDown(ValueIndex pValueIndex, KeyEventArgs pArgs)
		{
			switch (pArgs.Key)
			{
				case Key.Escape:
					this.SwitchToEditValue(ValueIndex.None, false);
					ValuesEditPopup.IsOpen = false;
					pArgs.Handled = true;
					break;

				case Key.Enter:
					{
						ValueIndex nextValueIndex =
						   pValueIndex == ValueIndex.None ? ValueIndex.Value1 :
						   pValueIndex == ValueIndex.Value1 ? this.valuesCount >= 2 ? ValueIndex.Value2 : ValueIndex.None :
						   pValueIndex == ValueIndex.Value2 ? this.valuesCount >= 3 ? ValueIndex.Value3 : ValueIndex.None :
						   pValueIndex == ValueIndex.Value3 ? this.valuesCount == 4 ? ValueIndex.Value4 : ValueIndex.None :
						   ValueIndex.None; // from value 4

						this.SwitchToEditValue(nextValueIndex, true);

						if (nextValueIndex == ValueIndex.None)
						{
							ValuesEditPopup.IsOpen = false;
						}

						pArgs.Handled = true;
					}
					break;

				case Key.Tab:
					{
						ValueIndex nextValueIndex =
						(
							Keyboard.IsKeyDown(Key.LeftShift)
						) ? (
							pValueIndex == ValueIndex.Value1 ?
								this.valuesCount == 4 ? ValueIndex.Value4 :
								this.valuesCount == 3 ? ValueIndex.Value3 :
								this.valuesCount == 2 ? ValueIndex.Value2 :
								ValueIndex.Value1 :
							pValueIndex == ValueIndex.Value2 ? ValueIndex.Value1 :
							pValueIndex == ValueIndex.Value3 ? ValueIndex.Value2 :
							pValueIndex == ValueIndex.Value4 ? ValueIndex.Value3 :
							ValueIndex.Value4
						) : (
							pValueIndex == ValueIndex.Value1 ? this.valuesCount >= 2 ? ValueIndex.Value2 : ValueIndex.Value1 :
							pValueIndex == ValueIndex.Value2 ? this.valuesCount >= 3 ? ValueIndex.Value3 : ValueIndex.Value1 :
							pValueIndex == ValueIndex.Value3 ? this.valuesCount >= 4 ? ValueIndex.Value4 : ValueIndex.Value1 :
							//pValueIndex == ValueIndex.Value4 ? ValueIndex.Value1 :
							ValueIndex.Value1
						);

						this.SwitchToEditValue(nextValueIndex, true);
						pArgs.Handled = true;
					}
					break;
			}

		}

		/* Private methods : Control event handlers */

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.ValuesCount = 3;

			this.selectedValueIndex = ValueIndex.None;

			Value1TextBlock.Text = this.GetValueString(ValueIndex.Value1);
			Value1TextBox.Text = this.GetValueString(ValueIndex.Value1);

			Value2TextBlock.Text = this.GetValueString(ValueIndex.Value2);
			Value2TextBox.Text = this.GetValueString(ValueIndex.Value2);

			Value3TextBlock.Text = this.GetValueString(ValueIndex.Value3);
			Value3TextBox.Text = this.GetValueString(ValueIndex.Value3);

			Value4TextBlock.Text = this.GetValueString(ValueIndex.Value4);
			Value4TextBox.Text = this.GetValueString(ValueIndex.Value4);

			this.SwitchToEditValue(ValueIndex.None, false);
		}
		private void UserControl_KeyDown(object sender, KeyEventArgs e)
		{
			this.ProcessValueTextBoxKeyDown(this.selectedValueIndex, e);
		}

		private void ValuesTextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				if (ValuesEditPopup.IsOpen)
				{
					ValuesEditPopup.IsOpen = false;
				}
				else
				{
					ValuesEditPopup.StaysOpen = true;
					ValuesEditPopup.IsOpen = true;
				}

				Mouse.Capture(ValuesTextBlock);
			}

			e.Handled = true;
		}
		private void ValuesTextBlock_MouseUp(object sender, MouseButtonEventArgs e)
		{
			Mouse.Capture(null);

			ValuesEditPopup.StaysOpen = false;

			e.Handled = true;
		}

		private void Value1TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				this.SwitchToEditValue(ValueIndex.Value1, true);
			}
		}
		private void Value2TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				this.SwitchToEditValue(ValueIndex.Value2, true);
			}
		}
		private void Value3TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				this.SwitchToEditValue(ValueIndex.Value3, true);
			}
		}
		private void Value4TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				this.SwitchToEditValue(ValueIndex.Value4, true);
			}
		}

		private void ValuesEditPopup_Closed(object sender, EventArgs e)
		{
			this.SwitchToEditValue(ValueIndex.None, true);
		}

	}
}