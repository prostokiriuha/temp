﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace km.wpf.controls
{
    public partial class ValueUpDown : UserControl
    {
        /* Static */

        public static readonly DependencyProperty ValueProperty;
        public static readonly DependencyProperty ValueIncProperty;
        public static readonly DependencyProperty ValueDecProperty;
        public static readonly DependencyProperty ValueMinProperty;
        public static readonly DependencyProperty ValueMaxProperty;
        public static readonly DependencyProperty ValueFormatProperty;

        /* Properties */

        public double Value
        {
            get
            {
                return (double)GetValue(ValueProperty);
            }
            set
            {
                //double v = this.ValuePrec == 0 ? value : (int)(value / this.ValuePrec) * this.ValuePrec;
                SetValue(ValueProperty, value);
            }
        }
        public double ValueInc
        {
            get
            {
                return (double)GetValue(ValueIncProperty);
            }
            set
            {
                SetValue(ValueIncProperty, value);
            }
        }
        public double ValueDec
        {
            get
            {
                return (double)GetValue(ValueDecProperty);
            }
            set
            {
                SetValue(ValueDecProperty, value);
            }
        }
        public double ValueMin
        {
            get
            {
                return (double)GetValue(ValueMinProperty);
            }
            set
            {
                SetValue(ValueMinProperty, value);
            }
        }
        public double ValueMax
        {
            get
            {
                return (double)GetValue(ValueMaxProperty);
            }
            set
            {
                SetValue(ValueMaxProperty, value);
            }
        }
        public string ValueFormat
        {
            get
            {
                return (string)GetValue(ValueFormatProperty);
            }
            set
            {
                SetValue(ValueFormatProperty, value);
            }
        }

        /* Events */
        public event EventHandler ValueChanged;
        public void NotifyValueChanged()
        {
            if (this.ValueChanged != null)
            {
                this.ValueChanged(this, null);
            }
        }

        //public double Text
        //{
        //    get
        //    {
        //        return this.text;
        //    }
        //}

        /* Constructors */

        static ValueUpDown()
        {
            ValueProperty = DependencyProperty.Register(
                "Value",
                typeof(double),
                typeof(ValueUpDown));
            ValueIncProperty = DependencyProperty.Register(
                "ValueInc",
                typeof(double),
                typeof(ValueUpDown));
            ValueDecProperty = DependencyProperty.Register(
                "ValueDec",
                typeof(double),
                typeof(ValueUpDown));
            ValueMinProperty = DependencyProperty.Register(
                "ValueMin",
                typeof(double),
                typeof(ValueUpDown));
            ValueMaxProperty = DependencyProperty.Register(
                "ValueMax",
                typeof(double),
                typeof(ValueUpDown));
            ValueFormatProperty = DependencyProperty.Register(
                "ValueFormat",
                typeof(string),
                typeof(ValueUpDown));
        }
        public ValueUpDown()
        {
            InitializeComponent();

            MainGrid.DataContext = this;

            this.ValueInc = 1;
            this.ValueDec = 1;
            this.ValueMin = double.MinValue;
            this.ValueMax = double.MaxValue;
            this.ValueFormat = "F1";
        }

        private void IncreaseValueButton_Click(object sender, RoutedEventArgs e)
        {
            this.Value = Math.Min(this.ValueMax, this.Value + this.ValueInc);
            this.NotifyValueChanged();
        }
        private void DecreaseValueButton_Click(object sender, RoutedEventArgs e)
        {
            this.Value = Math.Max(this.ValueMin, this.Value - this.ValueInc);
            this.NotifyValueChanged();
        }
    }
}