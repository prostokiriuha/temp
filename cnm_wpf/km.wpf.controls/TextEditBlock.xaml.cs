﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace km.wpf.controls
{
	public partial class TextEditBlock : UserControl
	{
		/* Static */

		public static readonly DependencyProperty TextProperty;

		/* Fields */

		private bool isEditing;

		/* Properties */

		public string Text
		{
			get
			{
				return (string)GetValue(TextProperty);
			}
			set
			{
				SetValue(TextProperty, value);
			}
		}

		/* Constructors */

		static TextEditBlock()
		{
			TextProperty = DependencyProperty.Register(
				"Text",
				typeof(string),
				typeof(TextEditBlock));
		}

		public TextEditBlock()
		{
			InitializeComponent();

			MainGrid.DataContext = this;

			this.EndEditText(false);
		}

		/* Private methods */

		private void StartEditText()
		{
			TextTextBlock.Visibility = System.Windows.Visibility.Hidden;
			TextTextBox.Visibility = System.Windows.Visibility.Visible;

			TextTextBox.Text = this.Text;
			TextTextBox.Focus();
			TextTextBox.SelectAll();

			this.isEditing = true;
		}
		private void EndEditText(bool pIsSaveChanges)
		{
			if (this.isEditing && pIsSaveChanges)
			{
				this.Text = TextTextBox.Text;
			}

			TextTextBox.Text = null;
			TextTextBox.Visibility = System.Windows.Visibility.Hidden;
			TextTextBlock.Visibility = System.Windows.Visibility.Visible;

			this.isEditing = false;
		}

		private void TextTextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			//Mouse.GetPosition((sender as FrameworkElement).Parent as IInputElement);
			if (e.ChangedButton == MouseButton.Left)
			{
				this.StartEditText();
				e.Handled = true;
			}

			Mouse.Capture(TextTextBox);
		}
		private void TextTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.EndEditText(true);
		}
		private void TextTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				this.EndEditText(false);
			}
			else if (e.Key == Key.Enter)
			{
				this.EndEditText(true);
			}
		}

	}
}