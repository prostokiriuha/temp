﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace km.wpf.controls
{
    public partial class ScrollBox : UserControl, INotifyPropertyChanged
    {
        /* Static */

        public static readonly DependencyProperty ItemsProperty;
        public static readonly DependencyProperty SelectedItemProperty;
        public static readonly DependencyProperty CanBeEmptyProperty;
        public static readonly DependencyProperty ItemIndexLabelWidthProperty;
        public static readonly DependencyProperty ItemInfoLabelWidthProperty;
        public static readonly DependencyProperty CustomItemProperty;

        private static readonly Brush BRUSH_CCE9FF = new SolidColorBrush(Color.FromRgb(204, 233, 255));
        private static readonly Brush BRUSH_E9E9E9 = new SolidColorBrush(Color.FromRgb(233, 233, 233));

        /* Fields */

        private int selectedIndex;
        private string itemLabelPath;

        /* Properties */

        public IEnumerable Items
        {
            get
            {
                return (IEnumerable)GetValue(ItemsProperty);
            }
            set
            {
                SetValue(ItemsProperty, value);
                this.UpdateItemsControls();
                this.NotifyPropertyChanged("Items");
            }
        }
        public object SelectedItem
        {
            get
            {
                return (object)GetValue(SelectedItemProperty);
            }
            set
            {
                SetValue(SelectedItemProperty, value);
                this.NotifyPropertyChanged("SelectedItem");
                this.NotifyPropertyChanged("SelectedItemLabel");
            }
        }
        public string ItemLabelPath
        {
            get
            {
                return this.itemLabelPath;
            }
            set
            {
                this.itemLabelPath = value;
                this.NotifyPropertyChanged("ItemLabelPath");
                this.NotifyPropertyChanged("SelectedItemLabel");
            }
        }
        public string SelectedItemLabel
        {
            get
            {
                object item = this.SelectedItem;
                return item == null ? null : (string)item.GetType().GetProperty(this.ItemLabelPath).GetValue(item, null);
            }
        }
        public UIElement CustomItem
        {
            get
            {
                return (UIElement)GetValue(CustomItemProperty);
            }
            set
            {
                SetValue(CustomItemProperty, value);
                CustomItemBorder.Child = value;
                this.NotifyPropertyChanged("CustomItem");
            }
        }

        public bool CanBeEmpty
        {
            get
            {
                return (bool)GetValue(CanBeEmptyProperty);
            }
            set
            {
                SetValue(CanBeEmptyProperty, value);
                this.NotifyPropertyChanged("CanBeEmpty");
            }
        }
        public double ItemInfoLabelWidth
        {
            get
            {
                return (double)GetValue(ItemInfoLabelWidthProperty);
            }
            set
            {
                SetValue(ItemInfoLabelWidthProperty, value);
                this.NotifyPropertyChanged("ItemInfoLabelWidth");
            }
        }

        /* Events */

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<object, object> SelectionChanged;

        /* Constructors */

        static ScrollBox()
        {
            ItemsProperty = DependencyProperty.Register(
                "Items",
                typeof(IEnumerable),
                typeof(ScrollBox));
            SelectedItemProperty = DependencyProperty.Register(
                "SelectedItem",
                typeof(object),
                typeof(ScrollBox));
            CanBeEmptyProperty = DependencyProperty.Register(
               "CanBeEmpty",
               typeof(bool),
               typeof(ScrollBox));
            ItemInfoLabelWidthProperty = DependencyProperty.Register(
                "ItemInfoLabelWidth",
                typeof(double),
                typeof(ScrollBox));
            ItemIndexLabelWidthProperty = DependencyProperty.Register(
                "ItemIndexLabelWidth",
                typeof(double),
                typeof(ScrollBox));
            CustomItemProperty = DependencyProperty.Register(
                "CustomItemProperty",
                typeof(UIElement),
                typeof(ScrollBox));
        }
        public ScrollBox()
        {
            InitializeComponent();

            MainGrid.DataContext = this;

            this.CanBeEmpty = true;
        }

        /* Private methods */

        private void UpdateControls()
        {
            int itemsCount = Items == null ? 0 : this.Items.Cast<object>().Count();
            if (itemsCount > 0)
            {
                PrevButton.IsEnabled = this.CanBeEmpty ? this.selectedIndex >= 0 : this.selectedIndex > 0;
                NextButton.IsEnabled = this.selectedIndex == -1 || this.selectedIndex < itemsCount - 1;
                ItemInfoTextBlock.Text = string.Format("{0}/{1}", this.selectedIndex + 1, itemsCount);
            }
            else
            {
                PrevButton.IsEnabled = false;
                NextButton.IsEnabled = false;
                ItemInfoTextBlock.Text = "-";
            }

            object item = this.SelectedItem;
            ItemNameTextBlock.Text = this.GetItemLabel(item);
        }
        private string GetItemLabel(object pItem)
        {
            string label = null;

            if (pItem != null)
            {
                System.Reflection.PropertyInfo proeprtyInfo =
                    this.ItemLabelPath == null ?
                    null :
                    pItem.GetType().GetProperty(this.ItemLabelPath);
                if (proeprtyInfo != null)
                {
                    label = (string)proeprtyInfo.GetValue(pItem, null);
                }
            }

            return label;
        }

        private void UpdateItemsControls()
        {
            IEnumerable<object> newItems = this.Items == null ? new List<object>() : this.Items.Cast<object>();

            int index = -1;
            if (this.CanBeEmpty)
            {
                newItems =
                    new List<object>(new object[] { null })
                    .Union(newItems);
                index--;
            }

            this.IsEnabled = newItems.Count() > 0;
            //this.items = newItems;

            ItemsItemsControl.ItemsSource = null;
            ItemsItemsControl.ItemsSource =
                newItems
                .ToDictionary(
                    x => ++index,
                    x => this.GetItemLabel(x));

            this.UpdateControls();
        }

        private void NotifyPropertyChanged(string pPropertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(pPropertyName));
            }
        }
        private void NotifySelectionChanged(object pItem)
        {
            if (this.SelectionChanged != null)
            {
                this.SelectionChanged(this, pItem);
            }
        }

        /* Private methods : event handlers */

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.CanBeEmpty ? this.selectedIndex >= 0 : this.selectedIndex > 0)
            {new ListBox().ItemsSource
                this.selectedIndex--;
                this.SelectedItem = this.Items.Cast<object>().[this.selectedIndex];
                this.UpdateControls();
                this.NotifySelectionChanged(this.SelectedItem);
            }
        }
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.selectedIndex < this.Items.Count() - 1)
            {
                this.selectedIndex++;
                this.SelectedItem = this.Items[this.selectedIndex];
                this.UpdateControls();
                this.NotifySelectionChanged(this.SelectedItem);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateControls();
        }

        private void ItemNameTextBlock_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
                ItemsPopup.StaysOpen = true;
                ItemsPopup.IsOpen = !ItemsPopup.IsOpen;
                Mouse.Capture(ItemNameTextBlock);

                e.Handled = true;
            }
        }
        private void ItemNameTextBlock_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
            {
                Mouse.Capture(null);
                ItemsPopup.StaysOpen = false;
                //e.Handled = true;
            }
        }

        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock textBlock = sender as TextBlock;
            int index = (int)textBlock.Tag;
            this.selectedIndex = index;
            ItemsPopup.IsOpen = false;
            e.Handled = true;
            this.UpdateControls();
            this.NotifySelectionChanged(this.SelectedItem);
        }

        /* Public methods */

        //public object SelectedItem
        //{
        //    return this.selectedIndex >= 0 && this.selectedIndex < this.GetItemsCount() ? this.items.ToArray()[this.selectedIndex] : null;
        //}

        ////public void SetSelectedGroupIndex(int pValue, bool pIsNotify)
        ////{
        ////    this.selectedIndex = pValue;

        ////    if (pIsNotify)
        ////    {
        ////        this.NotifySelectionChanged(this.SelectedItem);
        ////    }
        ////}
        //public int GetItemsCount()
        //{
        //    return this.items == null ? 0 : this.items.Count();
        //}

    }
}