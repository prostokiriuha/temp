﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace harp_tabs_test
{
    public class AA : Dictionary<char, char> { }

    public partial class MainWindow : Window
    {
        private const string INPUT_FILE_PATH = "./input.txt";
        private const string OUTPUT_FILE_PATH = "./output.txt";

        private const string VERSION = "BDAXSK";
        private const string EMAIL_L = "prostokiriuha@gmail.com";
        private const string EMAIL_U = "PROSTOKIRIUHA@GMAIL.COM";
        private const string HARPTABS = "BDAXSK";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void InputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            AA aa = new AA();
            OutputTextBox.Text = GetOutputText(InputTextBox.Text, aa);

            if (aa.ContainsKey('h'))
            {
                char v = aa['h'];
                
            }
        }

        private static readonly char[] keys = new char[] { 'o', 'b', 'h', 'n', 'm' };
        private static readonly int[] steps = new int[] { 16, 8, 4, 2, 1 };

        private static string GetOutputText(string inputText, AA aa)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<string> lines = inputText.Trim().Split(new char[] { '\n' }).Select(l => l.Trim());

            Queue<char> types = new Queue<char>();
            Queue<char> holes = new Queue<char>();
            Queue<int> sizes = new Queue<int>();

            try
            {
                foreach (string line in lines)
                {
                    if (string.IsNullOrEmpty(line))
                    {
                        sb.AppendLine();
                        continue;
                    }
                    else if (line.StartsWith("#"))
                    {
                        string[] words =
                            line
                            .TrimStart(new char[] { ' ', '#', '\t' })
                            .Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        int keysi = -1;

                        foreach (string word in words)
                        {
                            char w0 = word[0];
                            int wi = 1;
                            int size = 0;

                            if (w0 == 'k' || w0 == 'K')
                            {
                                if (word.Length < 2) continue;
                                char k = word[1];
                                for (int si = 0; si < keys.Length; si++)
                                {
                                    if (k == keys[si])
                                    {
                                        keysi = si;
                                        break;
                                    }
                                }

                                continue;
                            }
                            else if (w0 == 'x' || w0 == 'X')
                            {
                                if (word.Length < 2) continue;
                                types.Enqueue('x');
                                holes.Enqueue(' ');
                            }
                            else if (w0 == 's' || w0 == 'S')
                            {
                                if (word.Length < 2) continue;
                                types.Enqueue('s');
                                holes.Enqueue(' ');
                            }
                            else if (w0 == 'a' || w0 == 'A')
                            {
                                if (word.Length < 2) continue;
                                types.Enqueue('a');
                                holes.Enqueue(' ');
                            }
                            else
                            {
                                if (word.Length < 3) continue;
                                types.Enqueue(GetType(w0));
                                holes.Enqueue(word[1]);
                                wi++;
                            }

                            for (; wi < word.Length; wi++) size += GetSize(word[wi]);
                            sizes.Enqueue(size);
                        }

                        int min = sizes.Min();
                        int stepsi = -1;

                        for (int si = 0; si < keys.Length; si++)
                        {
                            int s = steps[si];
                            if (min % s == 0)
                            {
                                stepsi = si;
                                break;
                            }
                        }

                        if (stepsi == -1 && keysi == -1)
                        {
                            continue; // ???
                        }

                        //for (int si = 0; si < sizeNames.Length; si++)
                        //{
                        //    if (sizeNames[si] == key)
                        //    {
                        //        step = sizeSteps[si];
                        //        break;
                        //    }
                        //}

                        int maxsi = keysi > stepsi ? keysi : stepsi;
                        char key = keys[maxsi];
                        int step = steps[maxsi];

                        string str1 = "";
                        string str2 = "";

                        while (sizes.Any())
                        {
                            int size = sizes.Dequeue() / step;
                            char type = types.Dequeue();
                            char hole = holes.Dequeue();

                            string word1 = hole + new string(' ', size - 1);
                            string word2 =
                                type == 'a' ? new string('a', size) :
                                type == 'x' ? new string('x', size) :
                                type == 's' ? new string(' ', size) :
                                type + new string('a', size - 1);

                            str1 += word1;
                            str2 += word2;
                        }

                        int i = 0;
                        while (true)
                        {
                            if (i == 0)
                            {
                                str1 = " " + str1;
                                str2 = "|" + str2;
                                i += 9;
                            }
                            else
                            {
                                str1 = str1.Insert(i, "  ");
                                str2 = str2.Insert(i, " " + "|");
                                i += 10;
                            }

                            if (i >= str1.Length || i >= str2.Length) break;
                        }

                        string pre1 = "  ";
                        string pre2 = key + " ";

                        sb.AppendLine(pre1 + str1);
                        sb.AppendLine(pre2 + str2);
                        //sb.AppendLine();
                    }
                    else if (line.StartsWith("@"))
                    {
                        if (line.Length < 2) continue;
                        char c0 = line[1];
                        if (c0 == '@')
                        {
                            if (line.Length < 4) continue;
                            char ck = line[2];
                            char cv = line[3];
                            if (ck == 'h' || ck == 'H')
                            {
                                if (!aa.ContainsKey('h')) aa.Add('h', '0');
                                aa['h'] = cv;
                            }
                        }
                        else if (c0 == 'p')
                        {
                            sb.AppendLine(EMAIL_L);
                        }
                        else if (c0 == 'P')
                        {
                            sb.AppendLine(EMAIL_U);
                        }
                        else if (c0 == 'l' || c0 == 'L')
                        {
                            sb.AppendLine("");
                        }
                        else if (c0 == 'v' || c0 == 'V')
                        {
                            sb.AppendLine(VERSION);
                        }
                    }
                    else
                    {
                        sb.AppendLine(line);
                    }
                }
            }
            catch (Exception e)
            {
                sb.AppendLine();
                sb.AppendLine(e.Message);
                sb.AppendLine();
                sb.AppendLine(e.StackTrace);
            }

            return sb.ToString();
        }

        private static char GetType(char c)
        {
            switch (c)
            {
                case 'D': return 'd';
                case 'B': return 'b';
                case 'U': return 'u';
                case 'V': return 'v';
                case 'W': return 'w';
                case 'X': return 'x';
                default: return c;
            }
        }
        private static int GetSize(char c) //  in 1/16
        {
            switch (c)
            {
                case 'o': return 16;
                case 'b': return 8;
                case 'h': return 4;
                case 'n': return 2;
                case 'm': return 1;
                default: return 0;
            }
        }

        private void LoadInput(string filePath)
        {
            string text = null;
            try
            {
                text = File.ReadAllText(filePath);
            }
            catch
            {
            }
            finally
            {
                this.InputTextBox.Text = text;
            }
        }
        private void Save(string filePath, string text)
        {
            string tempFilePath = filePath + ".tmp";
            try
            {
                File.WriteAllText(tempFilePath, text);
                File.Delete(filePath);
                File.Move(tempFilePath, filePath);
            }
            catch
            {
            }
            finally
            {
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadInput(INPUT_FILE_PATH);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Save(INPUT_FILE_PATH, this.InputTextBox.Text);
            this.Save(OUTPUT_FILE_PATH, this.OutputTextBox.Text);
        }

        private void BackupInputButton_Click(object sender, RoutedEventArgs e)
        {
            this.Save(INPUT_FILE_PATH, this.InputTextBox.Text);
        }

        private void SaveInputButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog()
                {
                    Filter = "Text|*.txt|All|*.*",
                    FilterIndex = 0
                };
                if (dialog.ShowDialog() == true)
                {
                    this.Save(dialog.FileName, this.InputTextBox.Text);
                }
            }
            catch
            {
            }
        }

        private void LoadCopyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    Filter = "Text|*.txt|All|*.*",
                    FilterIndex = 0
                };
                if (dialog.ShowDialog() == true)
                {
                    this.LoadInput(dialog.FileName);
                }
            }
            catch (Exception ex)
            {
                InputTextBox.Text = ex.Message;
            }
        }

        private void SaveOutputButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog()
                {
                    Filter = "Text|*.txt|All|*.*",
                    FilterIndex = 0
                };
                if (dialog.ShowDialog() == true)
                {
                    this.Save(dialog.FileName, this.OutputTextBox.Text);
                }
            }
            catch (Exception ex)
            {
                InputTextBox.Text = ex.Message;
            }
        }
    }
}